<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_report_kunjungan_hotel extends CI_Model {
	var $tabel_a	= 'hotel';
	var $tabel_b	= 'ref_kategori';
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function refensiData($selectna, $tabelna)
	{
		$this->db->select($selectna);
		return $this->db->get($tabelna)->result_array();
	}
    public function DataHotelByKat($selectna, $where)
    {
        $this->db->select($selectna);
        $this->db->from($this->tabel_a);
        if ($where['kategori_id'] != null) {
            $this->db->where($where);
        }
        return $this->db->get()->result_array();
    }
    public function dataKunjungan($where)
    {
        $this->db->select('
            kunjungan_hotel.id,
            kunjungan_hotel.sum_wisnus,
            DATE_FORMAT(kunjungan_hotel.create_at, \'%m\') AS bulan,
            kunjungan_hotel.ratarata_menginap,
            hotel.nama,
            hotel.alamat,
            ref_kategori.id AS idKategori,
            ref_kategori.kategori
        ');
    	$this->db->from('kunjungan_hotel');
    	$this->db->join('hotel', 'kunjungan_hotel.hotel_id = hotel.id', 'left');
    	$this->db->join('ref_kategori', 'hotel.kategori_id = ref_kategori.id', 'left');
        $this->db->where($where);
        $this->db->order_by('create_at ASC, idKategori ASC, nama ASC');
        return $this->db->get()->result_array();
    }
    public function detilMancanegara($id)
    {
        $this->db->select('
                ref_negara.negara,
                detail_wisman.sum_wisman
        ');
        $this->db->from('detail_wisman');
        $this->db->join('kunjungan_hotel', 'kunjungan_hotel.id = detail_wisman.kunjungan_id', 'left');
        $this->db->join('ref_negara', 'ref_negara.id = detail_wisman.negara_id', 'left');
        $this->db->where('detail_wisman.kunjungan_id', $id);
        return $this->db->get()->result_array();
    }
    public function ttd($jabatan)
    {
        $this->db->select('pegawai.nip');
        $this->db->select('pegawai.nama');
        $this->db->select('ref_jabatan.jabatan');
        $this->db->from('pegawai');
        $this->db->join('ref_jabatan', 'ref_jabatan.id = pegawai.jabatan_id', 'left');
        $this->db->where('jabatan', $jabatan);
        $query = $this->db->get();
        return $query->row_array();
    }
}
