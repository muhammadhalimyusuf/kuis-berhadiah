<link href="<?php echo base_url(); ?>assets/app/plugin/datatables/datatables.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/select2/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/app/plugin/datatables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/report_kunjungan_hotel.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	<div class="row">
		
		<div class="col-md">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" onclick="reload_table('#list_data')"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<h4 class="panel-title"><?php echo ucwords($page); ?></h4>
				</div>
				<div class="panel-body">
					<div class="panel-body">
						<form action="<?= base_url('report_kunjungan_hotel/pdf'); ?>" id="form_laporan" class="form-inline" method="POST" target="_blank">
							<div class="form-group m-r-10">
								<label class="form-check-label">Filter :</label>
							</div>
							<div class="form-group m-r-10">
								<select class="form-control" id="report_kh_tahun" name="report_kh_tahun"></select>
							</div>
							<div class="form-group m-r-10">
								<select class="form-control" id="report_kh_bulan" name="report_kh_bulan"></select>
							</div>
							<div class="form-group m-r-10">
								<select class="form-control" id="report_kh_kategori" name="report_kh_kategori"></select>
							</div>
							<div class="form-group m-r-10">
								<select class="form-control" id="report_kh_hotel" name="report_kh_hotel">
								</select>
							</div>
							<div class="form-group m-r-10">
								<script>
									$( function() {
									    $( "#date" ).datepicker({
									    	showAnim: 'fadeIn',
									    	changeMonth: true,
									    	changeYear: true,
									    	dateFormat: 'dd MM yy'
									    }).keyup(function(e) {
										    if(e.keyCode == 8 || e.keyCode == 46) {
										        $.datepicker._clearDate(this);
										    }
										});
									});
								</script>
								<?php 
								$date = array(
									'name'			=> 'date',
									'id' 			=> 'date',
									'class'			=> 'form-control',
									'placeholder'	=> 'Tanggal Cetak Laporan',
									'autocomplete'  => 'off'
								);
								echo form_input($date);
								?>
							</div>
							<button type="reset" class="btn btn-sm btn-warning m-r-5" onclick="tombol_laporan()">Reset</button>
							<button type="submit" class="btn btn-sm btn-primary m-r-5">Buat Laporan</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
