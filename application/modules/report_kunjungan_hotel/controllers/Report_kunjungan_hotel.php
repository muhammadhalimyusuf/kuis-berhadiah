<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report_kunjungan_hotel extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_report_kunjungan_hotel','modelna');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index()
	{
		$this->mybreadcrumb->add('Laporan', base_url('Report_kunjungan_hotel'));
		$this->mybreadcrumb->add('Kunjungan Hotel', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'report_kunjungan_hotel';
		$data['page']        = 'Laporan Kunjungan Hotel';
		$data['content']     = 'content_report_kunjungan_hotel';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	private function _bulan($bln)
	{
		switch($bln) {
			case"01"; $bln="Januari"; break;
			case"02"; $bln="Februari"; break;
			case"03"; $bln="Maret"; break;
			case"04"; $bln="April"; break;
			case"05"; $bln="Mei"; break;
			case"06"; $bln="Juni"; break;
			case"07"; $bln="Juli"; break;
			case"08"; $bln="Agustus"; break;
			case"09"; $bln="September"; break;
			case"10"; $bln="Oktober"; break;
			case"11"; $bln="November"; break;
			case"12"; $bln="Desember"; break;
		}
		return $bln;
	}
	public function getDataHotelByKat()
	{
		if ($this->input->is_ajax_request()) {
			$selectna = array('id', 'nama');
			$id_kategori = $this->input->post('id_kat');
			$where = ['kategori_id' => $id_kategori];
			$data1[] = array('id' => '', 'kategori' => '');
			$data = $this->modelna->DataHotelByKat($selectna, $where);
			$gab = array_merge($data1, $data);
			echo json_encode($gab);
		} else {
			redirect('_404','refresh');
		}

	}
	public function getDataHotel()
	{
		if ($this->input->is_ajax_request()) {
			$selectna = array('id', 'nama');
			$data = $this->modelna->refensiData($selectna, 'hotel');
			$data1[] = array('id' => '', 'kategori' => '');
			$gab = array_merge($data1, $data);
			echo json_encode(array_values($gab));
		} else {
			redirect("_404","refresh");
		}
	}
	public function getDataKategori()
	{
		if ($this->input->is_ajax_request()) {
			$selectna = array('id', 'kategori');
			$data = $this->modelna->refensiData($selectna, 'ref_kategori');
			$data1[] = array('id' => '', 'kategori' => '');
			$gab = array_merge($data1, $data);
			echo json_encode($gab);
		} else {
			redirect('_404','refresh');
		}
		
	}
	public function pdf()
	{
		$year 	= $this->input->post('report_kh_tahun');
		$month 	= $this->input->post('report_kh_bulan');
		$kat 	= $this->input->post('report_kh_kategori');
		$hotel 	= $this->input->post('report_kh_hotel');
		$date   = $this->input->post('date'); 

		//jika ada tahun
		if ($year) {
			if ($month) {
				// ada bulan
				if ($kat) {
					if ($hotel) {
						// echo "tahun + bulan + kategori + hotel";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%m\') =' => $month,
							'ref_kategori.id' => $kat,
							'kunjungan_hotel.hotel_id' => $hotel
						];
					} else {
						// echo "tahun + bulan + kategori";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%m\') =' => $month,
							'ref_kategori.id' => $kat
						];
					}
				} else {
					if ($hotel) {
						// echo "tahun + bulan + hotel";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%m\') =' => $month,
							'kunjungan_hotel.hotel_id' => $hotel
						];
					} else {
						// echo "tahun + bulan";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%m\') =' => $month,
						];
					}
				}	
			} else {
				// tanpa bulan
				if ($kat) {
					if ($hotel) {
						// echo "tahun + kategori + hotel";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'ref_kategori.id' => $kat,
							'kunjungan_hotel.hotel_id' => $hotel
						];
					} else {
						// echo "tahun + kategori";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'ref_kategori.id' => $kat
						];
					}
				} else {
					// tanpa bulan - kategori
					if ($hotel) {
						// echo "tahun + hotel";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year, 
							'kunjungan_hotel.hotel_id' => $hotel
						];
					} else {
						// echo "tahun";
						$where = [
							'DATE_FORMAT(kunjungan_hotel.create_at, \'%Y\') =' => $year,
						];
					}
				}
			}

			$query = $this->modelna->dataKunjungan($where);
		
			$this->load->helper('pdf_helper');
			require_once(APPPATH.'tcpdf/tcpdf.php');
			// create new PDF document
			$pdf = new TCPDF('L', 'mm', [215,330]);

			$pdf->SetAuthor('disporabudpar.tsm');
			$pdf->SetTitle('Laporan Data Hotel '.date('d M Y'));
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetMargins(10, 10, 10);
			$pdf->SetAutoPageBreak(TRUE, 10);

			// add a page
			$pdf->AddPage();
			$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));

			$pdf->Image(K_PATH_IMAGES.'logokota.jpg', '23.5', '', 23, 0, 'JPG', '', 'C', false, 72, '', false, false, 0, false, false, false);
			$row_kop = [50, 260];
			$pdf->SetFont('helvetica','','14pt');
			$pdf->Cell($row_kop[0], 0);
			$pdf->Cell($row_kop[1], 0, 'PEMERINTAH KOTA TASIKMALAYA', 0, 1, 'C', 0, '', 0);
			
			$pdf->SetFont('helvetica','B','18pt');
			$pdf->Cell($row_kop[0], 0);
			$pdf->Cell($row_kop[1], 0, 'DINAS KEPEMUDAAN OLAHRAGA, KEBUDAYAAN DAN PARIWISATA', 0, 1, 'C', 0, '', 0);

			$pdf->SetFont('helvetica','','12pt');
			$pdf->Cell($row_kop[0], 0);
			$pdf->Cell($row_kop[1], 0, 'Jalan Dadaha, Kahuripan, Kecamatan Tawang Telp/Fax. (0265) 314376', 0, 1, 'C', 0, '', 0);

			$pdf->SetFont('helvetica','','12pt');
			$pdf->Cell($row_kop[0], 7);
			$pdf->Cell($row_kop[1], 7, 'Kode Pos 46124', 0, 1, 'R', 0, '', 0);

			$py = $pdf->getY();
			$pdf->Image(K_PATH_IMAGES.'garisL.jpg', '', $py, 310, 0, 'JPG', '', 'C', false, 75, '', false, false, 0, false, false, false);
			$pdf->ln();

			$text1 = 'DATA KUNJUNGAN HOTEL KOTA TASIKMALAYA TAHUN ';
			$pdf->SetFont('helvetica','B',14);
			$pdf->Cell(0, 8, $text1.$year, 0, 1, 'C');
			$pdf->Cell(0, 2);$pdf->ln();

			// -------------------------------- BEGIN HEADER -------------------------------------------
			$head = array('NO', 'BULAN', 'KATEGORI', 'NAMA HOTEL', 'ALAMAT', 'RATA-RATA LAMA TINGGAL ( hari )', 'PENGUNJUNG');
			$hrow = count($head);
			$w 	  = [12, 25, 31, 40, 71, 31, 100];
			$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
			$pdf->SetFillColor(229, 231, 233);
        	$pdf->SetTextColor(0);
			$pdf->SetFont('helvetica','B',12);
			$pdf->Cell($w[0], 28, $head[0], $border=1, $ln=0, $align='C', $fill=1, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
			$pdf->Cell($w[1], 28, $head[1], $border=1, $ln=0, $align='C', $fill=1, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
			$pdf->Cell($w[2], 28, $head[2], $border=1, $ln=0, $align='C', $fill=1, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
			$pdf->Cell($w[3], 28, $head[3], $border=1, $ln=0, $align='C', $fill=1, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
			$pdf->Cell($w[4], 28, $head[4], $border=1, $ln=0, $align='C', $fill=1, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');
			$pdf->SetFont('helvetica', '', 12);
			$html='<b><br/>RATA-RATA<br/>LAMA<br/>TINGGAL</b><br/><i>( hari )</i>';
			$pdf->writeHTMLCell($w[5], 28, '', '', $html, $border=1, $ln=0, $fill=1, $reseth=true, $align='C', $autopadding=true);
			$pdf->SetFont('helvetica', 'B', 12);
			$pdf->Cell($w[6], 7, $head[6], $border=1, $ln=0, $align='C', $fill=1, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M');

			$pdf->ln();
				$pdf->Cell(210, 7, '', 0, 0, 'C', 0);
			$pdf->SetFont('helvetica', '', 12);
			$html='<b><br/>NUSANTARA</b><br/><i>( orang )</i>';
			$pdf->writeHTMLCell(30, 21, '', '', $html, $border=1, $ln=0, $fill=1, $reseth=true, $align='C', $autopadding=true);
			$pdf->SetFont('helvetica', 'B', 12);
			$pdf->Cell(70, 7, 'MANCANEGARA', 1, 1, 'C', 1);
				$pdf->Cell(240, 7, '', 0, 0, 'C', 0);
			$pdf->SetFont('helvetica', 'B', 12);
			$pdf->MultiCell(40, 14, 'NAMA NEGARA', 1, 'C', 1, 0, '', '', true, 0, false, true, 14, 'M');
			$pdf->SetFont('helvetica', '', 12);
			$html='<b>JUMLAH</b><br/><i>( orang )</i>';
			$pdf->writeHTMLCell(30, 14, '', '', $html, $border=1, $ln=1, $fill=1, $reseth=true, $align='C', $autopadding=true);
			// ----------------------- END Header -----------------------------------------

			$w1 = [12, 25, 31, 40, 71, 31, 30, 40, 30];
			for ($i=0; $i < 9; $i++) { 
				$pdf->SetFont('helvetica', 'I', 12);
				$pdf->Cell($w1[$i], 0, $i+1, 1, 0, 'C', 1);
			}
			$pdf->ln();

			// -----------------------------------------------------------------------------
			$tbl = '<table border="0.2mm" cellpadding="1mm"><tbody>';

			$no=1;$totNus=0;$totMan=0;$avg=0;
			foreach ($query as $q) 
			{
				
				$bulan = $this->_bulan($q['bulan']);
				$tbl .= '
				<tr tr nobr="true">
				<td width="12mm" align="center">'.$no++.'.</td>
				<td width="25mm">'.$bulan.'</td>
				<td width="31mm">'.$q['kategori'].'</td>
				<td width="40mm">'.$q['nama'].'</td>
				<td width="71mm">'.$q['alamat'].'</td>
				<td width="31mm" align="center">'.$q['ratarata_menginap'].'</td>
				<td width="30mm" align="center">'.$q['sum_wisnus'].'</td>
				<td width="40mm">';
				$wisman = $this->modelna->detilMancanegara($q['id']);
				$o=1;
				foreach ($wisman as $key) {
					if ($key['negara'] != null) {
						$tbl .= '<br/>'.$o++.'. '.$key['negara'];
					} else {
						$tbl .= '-';
					}
				}
				$tbl .=	'</td><td width="30mm" align="center">';
				foreach ($wisman as $key1) {
					if ($key1['sum_wisman'] > 0) {
						$tbl .= '<br/>'.$key1['sum_wisman'];
					} else {
						$tbl .= '-';
					}
					$totMan += $key1['sum_wisman'];
				}
				$tbl .=	'</td></tr>';

				$totNus += $q['sum_wisnus'];
				$avg += $q['ratarata_menginap'];
			}
			
			$nomo = $no-1;
			$cNus = ($nomo != 0) ? $avg/$nomo : 0 ;
			
				$tbl .='<tr nobr="true" bgcolor="#E5E7E9">
							<td colspan="5" align="center" width="179mm">
								<b>T o t a l</b>
							</td>
							<td align="center" width="31mm"><b>'.number_format($cNus, 2, ',', '').'</b></td>
							<td align="center" width="30mm"><b>'.$totNus.'</b></td>
							<td align="center" width="40mm"></td>
							<td align="center" width="30mm"><b>'.$totMan.'</b></td>
						</tr>';
	        	$tbl .='</tbody></table>';

			$pdf->SetFont('helvetica', '', 12);
			$pdf->writeHTML($tbl);


			if ($date != "") {
				$dt 	= date('d', strtotime($date));
				$yr 	= date('Y', strtotime($date));
				$bln 	= date('m', strtotime($date));
			} else {
				$dt 	= date('d');
				$yr 	= date('Y');
				$bln 	= date('m');
			}
			
			$bln	= $this->_bulan($bln);
			$ttgl  = $dt.' '.$bln.' '.$yr;
			$kadis = $this->modelna->ttd('Kepala Dinas');
			$create= $this->modelna->ttd('Kepala Bidang Pariwisata');
			$ttd = '
				<table>
					<tr nobr="true">
						<td width="155mm">
							<p>
								Mengetahui,<br>
								'.$kadis['jabatan'].' Kepemudaan Olahraga,
								<br/>Kebudayaan dan Pariwisata Kota Tasikmalaya
								<br/><br/><br/><br/><br/>
								<b>'.strtoupper($kadis['nama']).'</b><br>
								NIP. '.$kadis['nip'].'
							</p>
						</td>
						<td width="155mm">
							<p>
								Tasikmalaya, '.$ttgl.'<br>
								'.$create['jabatan'].'
								<br/><br/><br/><br/><br/><br/>
								<b>'.strtoupper($create['nama']).'</b><br>
								NIP. '.$create['nip'].'
							</p>
						</td>
					</tr>
				</table>
			';

			$pdf->SetFont('helvetica', '', 12);
			$pdf->writeHTML($ttd, true, false, false, false, 'C');

			//Close and output PDF document
			$pdf->Output('Laporan Kunjungan Hotel '.date('dMY').'.pdf', 'I');
			
		} else {
			redirect('_404','refresh');
		}	
	}

	
} // End of class
