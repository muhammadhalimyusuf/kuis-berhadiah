<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('api_model');
	}
	public function index(){
		$this->load->view('api_view');
	
	}
	public function test(){
		$sensor1 = $this->input->get('sensor1');
		$sensor2 = $this->input->get('sensor2');

		echo $sensor1.'<br>'.$sensor2;
	}
	
}