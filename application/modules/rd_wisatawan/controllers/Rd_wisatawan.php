<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rd_wisatawan extends CI_Controller {
	var $tabelna = 'ref_wisatawan';
	var $idna    = 'id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_rd_wisatawan','rd_wisatawan');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->mybreadcrumb->add('Referensi Data', base_url('rd_wisatawan'));
		$this->mybreadcrumb->add('Wisataran', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'rd_wisatawan';
		$data['page']        = 'Wisatawan';
		$data['content']     = 'content_rd_wisatawan';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function get_data(){
		if($this->input->is_ajax_request()){
			$list = $this->rd_wisatawan->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $key) {
				$no++;
				$row    = array();
				$row[]  = $no.'.';
				$row[]  = ucwords($key->wisatawan);
				if ($key->status == 1) {
					$row[]  = '<center><button onclick="status_nonaktif(\''.$key->id.'\',\''.$key->wisatawan.'\')"  type="button" class="btn btn-icon btn-lime btn-sm"><i class="fa fa-unlock"></i></button></center>';
				}else{
					$row[]  = '<center><button onclick="status_aktif(\''.$key->id.'\',\''.$key->wisatawan.'\')" type="button" class="btn btn-icon btn-grey btn-sm"><i class="fa fa-lock"></i></button></center>';
				}
				$row[]  = '<center>
				<button type="button" onclick="edit_data('.$key->id.')" class="btn btn-info btn-icon btn-sm"><i class="fa fa-pencil-alt"></i></button>
				<button type="button" onclick="hapus_data(\''.$key->id.'\',\''.$key->wisatawan.'\')" class="btn btn-danger btn-icon btn-sm"><i class="fa fa-times"></i></button></center>
				';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->rd_wisatawan->count_all(),
				"recordsFiltered" => $this->rd_wisatawan->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function get_data_by_id(){
		if($this->input->is_ajax_request()){
			$id   = $this->input->post('id');
			$data = $this->rd_wisatawan->get_by_id($this->tabelna,$this->idna,$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_simpan(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'wisatawan'    => htmlspecialchars($this->input->post('wisatawan')),
				'status'      => '1',
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->rd_wisatawan->save($this->tabelna,$data);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil disimpan!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal disimpan!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_update(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'wisatawan'    => $this->input->post('wisatawan'),
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->rd_wisatawan->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _validate(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$agama = $this->input->post('wisatawan');
		if($agama == ''){
			$response['inputerror'][]   = 'wisatawan';
			$response['error_string'][] = 'Wisatawan harus diisi!';
			$response['status']         = 'invalid';
		}
		if($response['status'] === 'invalid'){
			echo json_encode($response);
			exit();
		}
	}
	public function status_aktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 1,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->rd_wisatawan->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function status_nonaktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->input->post('user_id');
			$data = array(
				'status'      => 0,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->rd_wisatawan->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_hapus(){
		if($this->input->is_ajax_request()){
			$id    = $this->input->post('id');
			$query = $this->rd_wisatawan->delete_by_id($this->tabelna,$this->idna,$id);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil dihapus!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal dihapus!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
}
