<link href="<?php echo base_url(); ?>assets/app/plugin/datatables/datatables.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/app/plugin/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/rd_wisatawan.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<h4 class="panel-title form-title">Form Input Data <?php echo ucwords($page); ?></h4>
				</div>
				<div class="panel-body">
					<form action="javascript:void(0)" id="form" class="needs-validation" novalidate>
						<input type="hidden" name="id"/> 
						<div class="panel-body">
							<div class="form-group">
								<label for="wisatawan">wisatawan</label>
								<input type="text" class="form-control" id="wisatawan" name="wisatawan" placeholder="Input Wisatawan">
								<div class="valid-feedback">Ok!</div>
								<div class="invalid-feedback"></div>
							</div>
						</div>
						<div class="panel-footer">
							<div class="pull-right">
								<button type="button" id="btnReset" onclick="page_reset()" class="btn btn-default btn-sm">Reset</button>
								<button type="button" id="btnSave" onclick="simpan()" class="btn btn-primary btn-sm">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" onclick="reload_table('#list_data')"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<h4 class="panel-title">List Data <?php echo ucwords($page); ?></h4>
				</div>
				<div class="panel-body">
					<div class="panel-body">
						<div class="table-responsive">
							<table id="list_data" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th><center>No</center></th>
										<th>Wisatawan</th>
										<th><center>Status</center></th>
										<th><center>Action</center></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>