<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="coder01" name="author" />
<title><?php echo $page.' | '.$app_description; ?></title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/app/images/logo/<?php echo $app_favicon; ?>">
<link href="<?php echo base_url(); ?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/animate/animate.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/css/default/style.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/css/default/style-responsive.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/css/default/theme/default.css" rel="stylesheet" id="theme" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app/plugin/toastr/toastr.min.css" />
</head>
<body class="pace-top bg-white">
  <div id="page-loader" class="fade show"><span class="spinner"></span></div>
  <div id="page-container" class="fade">
    <div class="login login-with-news-feed">
      <div class="news-feed">
        <div class="news-image" style="background-image: url(<?php echo base_url(); ?>assets/backend/img/login-bg/login-bg-11.jpg)"></div>
        <div class="news-caption">
          <h4 class="caption-title"><b><?php echo $app_name ?></b></h4>
          <p>
            <?php echo $app_description ?>
          </p>
        </div>
      </div>
      <div class="right-content">
        <div class="login-header">
          <div class="brand">
            <img src="<?php echo base_url(); ?>assets/logo-login.png ?>" class="img-rounded" alt="Logo"/>
            <!-- <img src="<?php echo base_url(); ?>assets/app/images/logo/<?php echo $app_logo_secondary; ?>" class="img-rounded" alt="Logo" width="100%" >  -->
             
            <span class="logo"></span> <b>DISPORA</b>BUDPAR
            <small>Kota Tasikmalaya - Jawa Barat</small>
          </div>
          <div class="icon">
            <i class="fa fa-sign-in"></i>
          </div>
        </div>
        <div class="login-content">
          <form  action="javascript:doLogin()" method="POST" class="margin-bottom-0 needs-validation" novalidate>
            <div class="form-group m-b-15">
              <input type="text" name="username" id="username" class="form-control form-control-lg" placeholder="Username" required autofocus/>
            </div>
            <div class="form-group m-b-15">
              <input type="password" name="password" id="password" class="form-control form-control-lg" placeholder="Password" required />
            </div>
            <div class="login-buttons">
              <button type="submit" class="btn btn-success btn-block btn-lg">Login</button>
            </div>
            <hr />
            <p class="text-center text-grey-darker">
              &copy; <?php echo $app_company.' All Right Reserved '.date('Y'); ?> 
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/jquery/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/plugins/js-cookie/js.cookie.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/js/theme/default.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/backend/js/apps.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/app/plugin/toastr/toastr.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/app/plugin/blockui/jquery.blockui.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/app/js/login.js" type="text/javascript"></script>
  <script>
    $(document).ready(function() {
      App.init();
    });
  </script>
</body>
</html>
