<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function login($username,$password){
		$password_dec = md5(md5($password));
		$this->db->select('*');
		$where = "app_login.username = '$username' and password = '$password_dec' and status = '1' ";
		$this->db->from('app_login');
		$this->db->where($where);
		$ceklogin = $this->db->get();	
		if(count($ceklogin->result())>0){
			$row_login = $ceklogin->row();
			$akses_id  = $row_login->akses_id;
			$user_id   = $row_login->user_id;
			$tipe_user = $row_login->tipe_user;
			$cekakses  = $this->db1->get_where('app_akses',array('id_akses'=>$akses_id));
			$row_akses = $cekakses->row();
			$level     = $row_akses->level;
			$menu_id   = $row_akses->menu_id;
			$id_akses  = $row_akses->id_akses;
			$apps = $this->app->apps();
			foreach ($apps->result() as $key) {
				$app_company        = $key->company;
				$app_name           = $key->name;
				$app_description    = $key->description;
				$app_address        = $key->address;
				$app_date           = date_create($key->date);
				$app_logo_primary   = $key->logo_primary;
				$app_logo_secondary = $key->logo_secondary;
				$app_favicon        = $key->favicon;
				$app_version        = $key->version;
				$app_status         = $key->status;
			}
			$profile = $this->profile($user_id, $tipe_user);
			foreach ($profile->result() as $key) {
				$pro_nama       = $key->nama;
				$pro_foto       = $key->foto;
				$pro_nama_akses = $key->nama_akses;
			}
			$newdata = array(
				'login'              => TRUE,
				'user_id'            => $user_id,
				'level'              => $level,
				'menu_id'            => $menu_id,
				'tipe_user'          => $tipe_user,
				'id_akses'           => $id_akses,
				'app_company'        => $app_company,
				'app_name'           => $app_name,
				'app_description'    => $app_description,
				'app_address'        => $app_address,
				'app_date'           => $app_date,
				'app_logo_primary'   => $app_logo_primary,
				'app_logo_secondary' => $app_logo_secondary,
				'app_favicon'        => $app_favicon,
				'app_version'        => $app_version,
				'app_status'         => $app_status,
				'pro_nama'           => $pro_nama,
				'pro_foto'           => $pro_foto,
				'pro_nama_akses'     => $pro_nama_akses,
			);
			$this->session->set_userdata($newdata);
			$this->status_login($user_id, 1);
			return true;
		}else{
			return false;
		}
	}
	public function status_login($user_id,$status){
		$data = array(
			'tanggal'      => date("Y-m-d H:i:s"),
			'status_login' => $status
		);
		$this->db->where('user_id', $user_id);
		$this->db->update('app_login', $data);
	}
	public function profile($user_id, $tipe_user){  
		$user_id   = $user_id;
		$tabel = $tipe_user;
		$this->db->select($tabel.'.nama');
		$this->db->select($tabel.'.foto');
		$this->db->select('app_akses.nama_akses');
		$this->db->from('app_login');
		$this->db->join('app_akses','app_login.akses_id = app_akses.id_akses');
		$this->db->join($tabel,'app_login.user_id = '.$tabel.'.id');
		$this->db->where($tabel.'.id',$user_id);
		$query = $this->db->get();
		return $query;
	}
}
