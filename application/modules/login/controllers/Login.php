<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('login_model');
	}
	public function index(){
		if($this->session->userdata('login')==TRUE){
			redirect('dashboard','refresh');
		}else{
			$this->_content();
		}
	}
	public function _content(){
		$apps = $this->app->apps();
		foreach ($apps->result() as $key) {
			$data['app_company']        = $key->company;
			$data['app_name']           = $key->name;
			$data['app_description']    = $key->description;
			$data['app_address']        = $key->address;
			$data['app_date']           = date_create($key->date);
			$data['app_logo_primary']   = $key->logo_primary;
			$data['app_logo_secondary'] = $key->logo_secondary;
			$data['app_favicon']        = $key->favicon;
			$data['app_version']        = $key->version;
			$data['app_status']         = $key->status;
		}
		$data['page']    = 'Login';
		$this->load->view('login_view',$data);
	}
	public function do_login(){
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('txtuser','Username','htmlspecialchars|trim|required|min_length[1]|max_length[100]');
			$this->form_validation->set_rules('txtpass','Password','htmlspecialchars|trim|required|min_length[1]|max_length[100]');
			if ($this->form_validation->run()==TRUE){
				$data['response'] = 'false';
				$username         = $this->app->anti($this->input->post('txtuser'));
				$password         = $this->app->anti($this->input->post('txtpass'));
				$result           = $this->login_model->login($username,$password); 
				if($result==TRUE){
					$data['response'] = 'true';
				}
			}else{
				$data['response'] = 'false';
			}
			if('IS_AJAX'){
				echo json_encode($data);
			}   
		}else{
			redirect("login",'refresh');
		}
	}
	public function logout(){
		if($this->input->is_ajax_request()){
			$this->login_model->status_login($this->session->userdata('user_id'),0);
			$this->session->sess_destroy();
		}else{
			redirect("_404",'refresh');
		}
	}
	public function generate(){
		$nganu = "admin";
		print_r(md5(md5($nganu)));
	}
}