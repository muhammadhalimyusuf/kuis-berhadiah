<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?php echo $page.' | '.$app_description; ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/backend/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/backend/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/backend/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/backend/css/default/style.min.css" rel="stylesheet" />
	<script src="<?php echo base_url();?>assets/backend/plugins/pace/pace.min.js"></script>
	<script src="<?php echo base_url();?>assets/backend/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url();?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url();?>assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url();?>assets/backend/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url();?>assets/backend/js/apps.min.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
</head>
<body class="pace-top">
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<div id="page-container" class="fade">
		<div class="error">
			<div class="error-code m-b-10">404</div>
			<div class="error-content">
				<div class="error-message">Ups halaman tidak ditemukan</div>
				<div class="error-desc m-b-30">
					Maaf halaman yang anda tuju tidak ditemukan. <br />
					Silahkan kembali ke halaman dashboard.
				</div>
				<div>
					<a href="<?php echo base_url();?>dashboard" class="btn btn-success p-l-20 p-r-20">Go Home</a>
				</div>
			</div>
		</div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	</div>
</body>
</html>