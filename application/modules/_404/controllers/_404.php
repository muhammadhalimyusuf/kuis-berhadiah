<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class _404 extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$apps = $this->app->apps();
		foreach ($apps->result() as $key) {
			$data['app_company']        = $key->company;
			$data['app_name']           = $key->name;
			$data['app_description']    = $key->description;
			$data['app_address']        = $key->address;
			$data['app_date']           = date_create($key->date);
			$data['app_logo_primary']   = $key->logo_primary;
			$data['app_logo_secondary'] = $key->logo_secondary;
			$data['app_favicon']        = $key->favicon;
			$data['app_version']        = $key->version;
			$data['app_status']         = $key->status;
		}
		$data['page']    = '404 Not Found';
		$this->output->set_status_header('404');
		$this->load->view('content_view',$data);
	}
}