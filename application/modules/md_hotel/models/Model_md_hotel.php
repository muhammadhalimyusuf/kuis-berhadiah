<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_md_hotel extends CI_Model {
	var $table 			= 'hotel';
	var $column_order 	= array(null,'nama','kategori','alamat','status',null); 
	var $column_search 	= array('nama','kategori','alamat');
	var $order = array('nama' => 'asc');
	private $_batchImport;

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	private function _get_datatables_query(){
		$this->db->select('hotel.id, hotel.nama, ref_kategori.kategori, hotel.alamat, hotel.status');
		$this->db->from($this->table);
		$this->db->join('ref_kategori', 'hotel.kategori_id = ref_kategori.id');
		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0) {
					$this->db->group_start();
					$this->db->like('lower('.$item.')', strtolower($_POST['search']['value']));
				}
				else{
					$this->db->or_like('lower('.$item.')', strtolower($_POST['search']['value']));
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		if(isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	public function get_datatables(){
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	public function count_filtered(){
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function count_all(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	public function save($table, $data){
		$this->db->insert($table, $data);
		return $this->db->affected_rows();
	}
	public function get_by_id($table,$where,$id){
		$this->db->from($table);
		$this->db->where($where,$id);
		$query = $this->db->get();
		return $query->row();
	}
	public function update($table, $data, $where){
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}
	public function delete_by_id($table,$where,$id){
		$this->db->where($where, $id);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}
	public function setBatchImport($batchImport) {
		$this->_batchImport = $batchImport;
	}
	public function importData() {
		$data = $this->_batchImport;
		$this->db->insert_batch('hotel', $data);
	}
	public function get_kategori(){
		$query = $this->db->get('ref_kategori');
		return $query->result();
	}
	public function cekidhotel(){
        $query = $this->db->query("SELECT MAX(id) as idhotel from hotel");
        $hasil = $query->row();
        return $hasil->idhotel;
    }
}
