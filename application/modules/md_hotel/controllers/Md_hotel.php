<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/vendor/autoload.php';
// $cache = new MyCustomPsr16Implementation();
// \PhpOffice\PhpSpreadsheet\Settings::setCache($cache);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
class Md_hotel extends CI_Controller {
	var $tabelna = 'hotel';
	var $idna    = 'id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_md_hotel','md_hotel');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->mybreadcrumb->add('Hotel', base_url('md_hotel'));
		$this->mybreadcrumb->add('Master Data', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'md_hotel';
		$data['page']        = 'Hotel';
		$data['content']     = 'content_md_hotel';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function get_kategori(){
		if($this->input->is_ajax_request()){
			$data = $this->md_hotel->get_kategori();
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function get_data(){
		if($this->input->is_ajax_request()){
			$list = $this->md_hotel->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $key) {
				$no++;
				$row    = array();
				$row[]  = $no.'.';
				$row[]  = ucwords($key->nama);
				$row[]  = ucwords($key->kategori);
				$row[]  = $key->alamat;
				if ($key->status == 1) {
					$row[]  = '<center><button onclick="status_nonaktif(\''.$key->id.'\',\''.ucwords($key->nama).'\')"  type="button" class="btn btn-icon btn-lime btn-sm" title="Hotel '.ucwords($key->nama).' Aktif"><i class="fa fa-unlock"></i></button></center>';
				}else{
					$row[]  = '<center><button onclick="status_aktif(\''.$key->id.'\',\''.ucwords($key->nama).'\')" type="button" class="btn btn-icon btn-grey btn-sm" title="Hotel '.ucwords($key->nama).' Nonaktif"><i class="fa fa-lock"></i></button></center>';
				}
				$row[]  = '<center><button type="button" onclick="edit_data(\''.$key->id.'\')" class="btn btn-info btn-icon btn-sm" data-toggle="modal" data-target="#modalHotel" title="Edit Hotel '.ucwords($key->nama).'"><i class="fa fa-pencil-alt"></i></button>
				<button type="button" onclick="hapus_data(\''.$key->id.'\',\'Hotel '.ucwords($key->nama).'\')" class="btn btn-danger btn-icon btn-sm" title="Hapus Hotel '.ucwords($key->nama).'"><i class="fa fa-times"></i></button></center>
				';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->md_hotel->count_all(),
				"recordsFiltered" => $this->md_hotel->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function get_data_by_id(){
		if($this->input->is_ajax_request()){
			$id   = $this->input->post('id');
			$data = $this->md_hotel->get_by_id($this->tabelna,$this->idna,$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _cekidhotel(){
		$dariDB = $this->md_hotel->cekidhotel();
		$nourut = substr($dariDB, 4, 3);
		$idbaru = $nourut + 1;
		$idnew	= sprintf("%03s", $idbaru);
		return $idnew;
	}
	public function process_simpan(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'id'			=> 'HTL-'.$this->_cekidhotel(),
				'nama'       	=> htmlspecialchars($this->input->post('nama_hotel')),
				'kategori_id'	=> htmlspecialchars($this->input->post('kategori_hotel')),
				'alamat'		=> htmlspecialchars($this->input->post('alamat_hotel')),
				'last_update' 	=> date('Y-m-d H:i:s'),
				'user_id'     	=> $user_id,
				'status'		=> 1
			);
			$query = $this->md_hotel->save($this->tabelna,$data);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil disimpan!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal disimpan!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_update(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$id      = $this->input->post('id');
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'nama'       	=> htmlspecialchars($this->input->post('nama_hotel')),
				'kategori_id'	=> htmlspecialchars($this->input->post('kategori_hotel')),
				'alamat'		=> htmlspecialchars($this->input->post('alamat_hotel')),
				'last_update' 	=> date('Y-m-d H:i:s'),
				'user_id'     	=> $user_id
			);
			$query = $this->md_hotel->update('hotel', $data, array('id' => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
				$response['data']	= $query;
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _validate(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_hotel', 'Nama Hotel', 'trim|required|min_length[3]|max_length[12]');
		$this->form_validation->set_rules('kategori_hotel', 'Kategori Hotel', 'required');
		$this->form_validation->set_rules('alamat_hotel', 'Alamat Hotel', 'trim|required|min_length[10]');
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$response['data']		  = array();
		if ($this->form_validation->run() == FALSE) {
			$response['status'] = 'invalid';
			$response['inputerror']   = [
				'nama_hotel',
				'kategori_hotel',
				'alamat_hotel'
			];
			$response['error_string'] = [
				form_error('nama_hotel'),
				form_error('kategori_hotel'),
				form_error('alamat_hotel')
			];
		}
		if($response['status'] === 'invalid') {
			echo json_encode($response);
			exit();
		}
	}
	public function status_aktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 1,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->md_hotel->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function status_nonaktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 0,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->md_hotel->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_hapus(){
		if($this->input->is_ajax_request()){
			$id    = $this->input->post('id');
			$query = $this->md_hotel->delete_by_id($this->tabelna,$this->idna,$id);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil dihapus!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal dihapus!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
}
