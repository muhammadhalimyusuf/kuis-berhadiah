<link href="<?php echo base_url(); ?>assets/app/plugin/datatables/datatables.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/app/plugin/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/md_hotel.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" onclick="reload_table('#list_data')"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<h4 class="panel-title">List Data <?php echo ucwords($page); ?></h4>
				</div>
				<div class="panel-body">
					<div class="panel-body">
						<div  id=myGroup>
							
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight">
									<button type="button" class="btn btn-info btn-sm" onclick="tambah_data()" data-toggle="modal" data-target="#modalHotel">Tambah</button>
								</div>
							</div>

						</div>
						<div class="table-responsive mt-3">
							<table id="list_data" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th><center>No</center></th>
										<th>Nama</th>
										<th>Kategori</th>
										<th>Alamat</th>
										<th>Status</th>
										<th><center>Action</center></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalHotel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalHotelTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<form action="javascript:void(0)" id="form" class="needs-validation" novalidate>
					<input type="hidden" name="id"/>
					<div class="form-group">
						<label for="nama_hotel">Nama Hotel</label>
						<input type="text" class="form-control" id="nama_hotel" name="nama_hotel" placeholder="Input Nama Hotel">
						<div class="valid-feedback">Ok!</div>
						<div class="invalid-feedback"></div>
					</div>

					<div class="form-group">
						<label for="kategori_hotel">Kategori Hotel</label>
						<select class="form-control" id="kategori_hotel" name="kategori_hotel"></select>
						<div class="valid-feedback">Ok!</div>
						<div class="invalid-feedback"></div>
					</div>

					<div class="form-group">
						<label for="alamat_hotel">Alamat Hotel</label>
						<input type="text" class="form-control" id="alamat_hotel" name="alamat_hotel" placeholder="Input Alamat Hotel">
						<div class="valid-feedback">Ok!</div>
						<div class="invalid-feedback"></div>
					</div>
					</form>
				</div>

				<div class="modal-footer">
					<button type="button" id="btnReset" onclick="page_reset()" class="btn btn-default btn-sm">Reset</button>
					<button type="button" id="btnSave" onclick="simpan()" class="btn btn-primary btn-sm">Simpan</button>
				</div>
		</div>
	</div>
</div>
