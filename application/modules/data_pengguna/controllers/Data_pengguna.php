<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/vendor/autoload.php';
// $cache = new MyCustomPsr16Implementation();
// \PhpOffice\PhpSpreadsheet\Settings::setCache($cache);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
class data_pengguna extends CI_Controller {
	var $tabelna = 'admin';
	var $idna    = 'id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_data_pengguna','data_pengguna');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->mybreadcrumb->add('Data Pengguna', base_url('data_pengguna'));
		$this->mybreadcrumb->add('Manajemen Sistem', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'data_pengguna';
		$data['page']        = 'Data Pengguna';
		$data['content']     = 'content_data_pengguna';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function get_data(){
		if($this->input->is_ajax_request()){
			$list = $this->data_pengguna->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $key) {
				$no++;
				$row    = array();
				$row[]  = $no.'.';
				$row[]  = ucwords($key->nama_menu);
				$row[]  = ucwords($key->link);
				$row[]  = '<center>
				<button type="button" onclick="edit_data(\''.$key->id_menu.'\')" class="btn btn-info btn-icon btn-sm" data-toggle="modal" data-target="#modalMenu" title="Edit Menu '.ucwords($key->nama_menu).'"><i class="fa fa-pencil-alt"></i></button>
				<button type="button" onclick="hapus_data(\''.$key->id_menu.'\',\'Menu '.ucwords($key->nama_menu).'\')" class="btn btn-danger btn-icon btn-sm" title="Hapus Menu '.ucwords($key->nama_menu).'"><i class="fa fa-times"></i></button></center>
				';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->data_pengguna->count_all(),
				"recordsFiltered" => $this->data_pengguna->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function get_data_by_id(){
		if($this->input->is_ajax_request()){
			$id   = $this->input->post('id');
			$data = $this->data_pengguna->get_by_id($this->tabelna,$this->idna,$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_simpan(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			// $user_id = $this->session->userdata('user_id');
			if ($this->input->post('kel_menu') < 0) {
				$parent = 0;
				$urutan = $this->cekurutan($parent);
				$data = array(
					'nama_menu'     => htmlspecialchars($this->input->post('nama_menu')),
					'urutan'		=> $urutan,
					'parent'		=> $parent,
					'anak'			=> 1,
					'level'			=> 1,
					'status'		=> 1,
					// 'last_update' 	=> date('Y-m-d H:i:s'),
					// 'user_id'     => $user_id
				);
			} else {

			}
			$query = $this->data_pengguna->save($this->tabelna,$data);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil disimpan!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal disimpan!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_update(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$id      = $this->input->post('id');
			// $user_id = $this->session->userdata('user_id');
			$data = array(
				'nama'       	=> htmlspecialchars($this->input->post('nama_hotel')),
				'kategori_id'	=> htmlspecialchars($this->input->post('kategori_hotel')),
				'alamat'		=> htmlspecialchars($this->input->post('alamat_hotel'))
			);
			$query = $this->data_pengguna->update('hotel', $data, array('id' => $id));
			if($query > 0 or $query == 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
				$response['data']	= $query;
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _validate(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_menu', 'Nama Menu', 'trim|required|min_length[3]|max_length[12]');
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$response['data']		  = array();
		if ($this->form_validation->run() == FALSE) {
			$response['status'] = 'invalid';
			$response['inputerror']   = [
				'nama_menu',
				'kategori_hotel',
				'alamat_hotel'
			];
			$response['error_string'] = [
				form_error('nama_menu'),
				form_error('kategori_hotel'),
				form_error('alamat_hotel')
			];
		}
		if($response['status'] === 'invalid') {
			echo json_encode($response);
			exit();
		}
	}
	public function status_aktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 1,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->data_pengguna->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function status_nonaktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->input->post('user_id');
			$data = array(
				'status'      => 0,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->data_pengguna->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_hapus(){
		if($this->input->is_ajax_request()){
			$id    = $this->input->post('id');
			$query = $this->data_pengguna->delete_by_id($this->tabelna,$this->idna,$id);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil dihapus!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal dihapus!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
}
