<link href="<?php echo base_url(); ?>assets/app/plugin/datatables/datatables.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/app/plugin/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/data_pengguna.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" onclick="reload_table('#list_data')"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<h4 class="panel-title">List Data <?php echo ucwords($page); ?></h4>
				</div>
				<div class="panel-body">
					<div class="panel-body">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

