<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Employes extends CI_Controller {
	var $tabelna = 'pegawai';
	var $idna    = 'id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_employes','employes');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->mybreadcrumb->add('Pegawai', base_url('employes'));
		$this->mybreadcrumb->add('Master Data', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'employes';
		$data['page']        = 'Pegawai';
		$data['content']     = 'content_employes';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function getJabatan()
	{
		if($this->input->is_ajax_request()){
			$data = $this->employes->jabatan('ref_jabatan');
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function getBidang()
	{
		if($this->input->is_ajax_request()){
			$data = $this->employes->referensi('ref_bidang');
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function get_data(){
		if($this->input->is_ajax_request()){
			$list = $this->employes->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $key) {
				$no++;
				$row    = array();
				$row[]  = $no.'.';
				$row[]  = $key->nip;
				$row[]  = ucwords($key->nama);
				$row[]  = ucwords($key->bidang);
				$row[]  = ucwords($key->jabatan);
				if ($key->stsPegawai == 1) {
					$row[]  = '<center><button onclick="status_nonaktif(\''.$key->idPegawai.'\',\''.ucwords($key->nama).'\')"  type="button" class="btn btn-icon btn-lime btn-sm" title="Hotel '.ucwords($key->nama).' Aktif"><i class="fa fa-unlock"></i></button></center>';
				}else{
					$row[]  = '<center><button onclick="status_aktif(\''.$key->idPegawai.'\',\''.ucwords($key->nama).'\')" type="button" class="btn btn-icon btn-grey btn-sm" title="Hotel '.ucwords($key->nama).' Nonaktif"><i class="fa fa-lock"></i></button></center>';
				}
				$row[]  = '<center><button type="button" onclick="edit_data(\''.$key->idPegawai.'\')" class="btn btn-info btn-icon btn-sm" data-toggle="modal" data-target="#modalPegawai" title="Edit Hotel '.ucwords($key->nama).'"><i class="fa fa-pencil-alt"></i></button>
				<button type="button" onclick="hapus_data(\''.$key->idPegawai.'\',\'Hotel '.ucwords($key->nama).'\')" class="btn btn-danger btn-icon btn-sm" title="Hapus Hotel '.ucwords($key->nama).'"><i class="fa fa-times"></i></button></center>
				';
				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->employes->count_all(),
				"recordsFiltered" => $this->employes->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function get_data_by_id(){
		if($this->input->is_ajax_request()){
			$id   = $this->input->post('id');
			$data = $this->employes->get_by_id($this->tabelna,$this->idna,$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_simpan(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$user_id   = $this->session->userdata('user_id');
			$data = array(
				'nip'       	=> htmlspecialchars($this->input->post('nip')),
				'nama'			=> htmlspecialchars($this->input->post('nama')),
				'bidang_id'		=> $this->input->post('bidang'),
				'jabatan_id' 	=> $this->input->post('jabatan'),
				'status'		=> 1,
				'last_update'	=> date('Y-m-d H:i:s'),
				'user_id'     	=> $user_id,
			);
			$query = $this->employes->save($this->tabelna,$data);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil disimpan!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal disimpan!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_update(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$user_id   = $this->session->userdata('user_id');
			$id      = $this->input->post('id');
			$data = array(
				'nip'       	=> htmlspecialchars($this->input->post('nip')),
				'nama'			=> htmlspecialchars($this->input->post('nama')),
				'bidang_id'		=> $this->input->post('bidang'),
				'jabatan_id' 	=> $this->input->post('jabatan'),
				'last_update' 	=> date('Y-m-d H:i:s'),
				'user_id'     	=> $user_id
			);
			$query = $this->employes->update($this->tabelna, $data, ['id' => $id]);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
				$response['data']	= $query;
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _validate(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		
		$this->form_validation->set_rules('nip', 'NIP', 'trim|required|min_length[18]|max_length[25]');
		$this->form_validation->set_rules('nama', 'Nama', 'required|min_length[3]|trim');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('bidang', 'Bidang', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			
			$ni = ['nip', 'nama', 'jabatan', 'bidang'];
			
			for ($i=0; $i < count($ni) ; $i++) { 
				$inputan = form_error($ni[$i]);
				if ($inputan !='') {
					$response['inputerror'][]   = $ni[$i];
					$response['error_string'][] = $inputan;
					$response['status']         = 'invalid';
				}
			}
		}
		if($response['status'] === 'invalid') {
			echo json_encode($response);
			exit();
		}
	}
	public function status_aktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 1,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->employes->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function status_nonaktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 0,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->employes->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_hapus(){
		if($this->input->is_ajax_request()){
			$id    = $this->input->post('id');
			$query = $this->employes->delete_by_id($this->tabelna,$this->idna,$id);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil dihapus!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal dihapus!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
}
