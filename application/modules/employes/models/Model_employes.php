<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_employes extends CI_Model {
	var $table 			= 'pegawai';
	var $column_order 	= array(null,'nik','nama','bidang','jabatan','status',null); 
	var $column_search 	= array('nik','nama','bidang','jabatan');
	var $order = array('nama' => 'asc');
	private $_batchImport;

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function referensi($table){
		$query = $this->db->get($table);
		return $query->result();
	}
	public function jabatan($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$where = "id NOT IN (SELECT jabatan_id FROM pegawai)";
		$this->db->where($where);
		return $this->db->get()->result();
	}
	private function _get_datatables_query(){
		$this->db->select('
				pegawai.id AS idPegawai,
				pegawai.nip,
				pegawai.nama,
				pegawai.status AS stsPegawai,
				ref_bidang.id,
				ref_bidang.bidang,
				ref_jabatan.id,
				ref_jabatan.jabatan
			');
		$this->db->from($this->table);
		$this->db->join('ref_bidang', 'pegawai.bidang_id = ref_bidang.id', 'left');
		$this->db->join('ref_jabatan', 'pegawai.jabatan_id = ref_jabatan.id', 'left');
		$this->db->order_by('ref_jabatan.id ASC, ref_bidang.id ASC');
		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0) {
					$this->db->group_start();
					$this->db->like('lower('.$item.')', strtolower($_POST['search']['value']));
				}
				else{
					$this->db->or_like('lower('.$item.')', strtolower($_POST['search']['value']));
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		if(isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	public function get_datatables(){
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	public function count_filtered(){
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function count_all(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	public function save($table, $data){
		$this->db->insert($table, $data);
		return $this->db->affected_rows();
	}
	public function get_by_id($table,$where,$id){
		$this->db->from($table);
		$this->db->where($where,$id);
		$query = $this->db->get();
		return $query->row();
	}
	public function update($table, $data, $where){
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}
	public function delete_by_id($table,$where,$id){
		$this->db->where($where, $id);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}
}
