<link href="<?php echo base_url(); ?>assets/app/plugin/datatables/datatables.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/parsley/parsley.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/backend/plugins/sweetalert2/sweetalert2.min.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/app/plugin/datatables/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/validate/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/in_hotel.js"></script>
<style type="text/css">
.select2 {
	width:100%!important;
}
.has-error .select2-selection {
    border-color:red !important;
}
.is-invalid .select2-selection,
.needs-validation ~ span > .select2-dropdown{
  border-color:red !important;
}
.is-valid .select2-selection,
.needs-validation ~ span > .select2-dropdown{
  border-color:#00acac !important;
}
</style>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" onclick="reload_table('#list_data')"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					</div>
					<h4 class="panel-title">List Data <?php echo ucwords($page); ?></h4>
				</div>
				<div class="panel-body">
					<div class="panel-body">
						<div  id=myGroup>
							
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight">
									<button id="tes1" type="button" class="btn btn-info btn-sm" onclick="tambah_data()" data-toggle="modal" data-target="#modalKunjungan">Tambah</button>
								</div>
							</div>

						</div>
						<div class="table-responsive mt-3">
							<table id="list_data" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th><center>No</center></th>
										<th>Nama Hotel</th>
										<th>Bulan</th>
										<th>Jumlah Pengunjung</th>
										<th><center>Action</center></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalKunjungan" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalKunjunganTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<form action="javascript:void(0)" id="form" class="needs-validation" novalidate>
						
						<div class="form-group">
							<?php echo form_label('Nama Hotel', 'hhotel');
								$aksesna = $this->session->userdata('id_akses');
							if ($aksesna == '4'): $user_id = $this->session->userdata('user_id'); ?>
								<input type="text" name="pilih_hotel" id="hhotel" class="form-control" disabled value="<?php echo($this->in_hotel->data_hotel_byid()->nama); ?>" />
								<input type="hidden" name="hotel_id" id="idhot" value="<?php echo($this->in_hotel->data_hotel_byid()->id); ?>" />
							<?php else: ?>
								<style>.ui-autocomplete {max-height: 100px; overflow-y: auto; overflow-x: hidden; z-index: 2150000000;}</style>
								<script>
									$(document).ready(function(){
										// Initialize 
										$( "#hhotel" ).autocomplete({
											source: function( request, response ) {
											// Fetch data
												$.ajax({
													url: $BASE_URL+"input_kunjungan_hotel/hotelList",
													type: 'post',
													dataType: "json",
													data: { search: request.term },
													success: function( data ) {
														response( data );
													}
												});
											},
											select: function (event, ui) {
												// Set selection
												$('#hhotel').val(ui.item.label); // display the selected text
												$('#idhot').val(ui.item.value); // save selected id to input
												return false;
											}
										});
									});
								</script>
								<?php $attrh = array(
									'name'			=> 'pilih_hotel',
									'id' 			=> 'hhotel',
									'class'			=> 'form-control',
									'placeholder'	=> 'Masukan Nama Hotel',
									'type' 			=> 'text',
									'data-parsley-required' => 'true',
								);
								echo form_input($attrh); ?>
								<div class="valid-feedback">Ok!</div>
								<div class="invalid-feedback"></div>
								<input type="hidden" name="hotel_id" id="idhot"/>
							<?php endif ?>
						</div>

						<div class="form-group">
							<script>
								$( function() {
								    $( "#date" ).datepicker({
								    	showAnim: 'fadeIn',
								    	changeMonth: true,
								    	changeYear: true,
								    	dateFormat: 'dd MM yy'
								    });
								});
							</script>
							<?php 
							echo form_label('Tanggal Laporan', 'date');
							$date = array(
								'name'			=> 'date',
								'id' 			=> 'date',
								'class'			=> 'form-control',
								'placeholder'	=> 'Isikan Tanggal Laporan',
							);
							echo form_input($date);
							?>
							<div class="valid-feedback">Ok!</div>
							<div class="invalid-feedback"></div>
						</div>

						<div class="form-group">
							<?php 
							echo form_label('Rata-rata Lama Tinggal', 'rataTinggal');
							$attrrata = array(
								'id' 			=> 'rataTinggal',
								'class'			=> 'form-control',
								'data-parsley-required' => 'true',
							); 
							$options = array(
								'1' => '1 Hari',
								'2' => '2 Hari',
								'3' => '3 Hari',
								'4' => '4 Hari',
								'5' => '5 Hari',
								'6' => '6 Hari',
								'7' => '7 Hari', 
							);
							echo form_dropdown('rata_tinggal', $options, '', $attrrata);
							?>
							<div class="valid-feedback">Ok!</div>
							<div class="invalid-feedback"></div>
						</div>

						<div class="form-group">
							<?php 
							echo form_label('Jumlah Wisatawan Nusantara', 'wisatawanNusantara');
							$attrInNusa = array(
								'name'			=> 'wisnus',
								'id' 			=> 'wisatawanNusantara',
								'class'			=> 'form-control',
								'placeholder'	=> 'Isikan Jumlah Pengunjung Wisnus',
								'type' 			=> 'number',
								'data-parsley-required' => 'true',
								'data-parsley-type' => 'number'
							); 
							echo form_input($attrInNusa);
							?>
							<div class="valid-feedback">Ok!</div>
							<div class="invalid-feedback"></div>
						</div>

						<div class="form-group">
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="cekManca" onclick="myFunction();" name="checkmanca" value="1"/>
								<label for="cekManca">Wisatawan Mancanegara ?</label>
							</div>
						</div>
						<div class="form-group" id="formManca">
							<div class="table-responsive">
								<table class="table table-striped m-b-0" id="item_table">
									<thead>
										<tr>
											<th width="45%"><center>Pilih Negara</center></th>
											<th width="45%"><center>Wisatawan Mancanegara</center></th>
											<th width="10%"><center><button type="button" name="add" class="btn btn-primary btn-sm add">Tambah</button></center></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>

								<script>
									$(document).ready(function () {
										$(document).on('click', '.add', function(){
									        $.ajax({
									        	url: $BASE_URL+modules+"/get_data_negara",
									        	dataType: 'json',
									        	success: function (datas) {
									        		var negara = $.map(datas, function (obj) {
						                                obj.id = obj.id || obj.id;
						                                obj.text = obj.text || obj.negara;
						                                return obj;
						                            });	
											        $('.s_wisman').select2({
											        	dropdownParent: $("#modalKunjungan"),
												        placeholder: "Pilih Negara",
												        data: negara
												    });
									        	},
									        	error: function (xhr, ajaxOptions, thrownError) {
									        		alert("error");
									        	}
									        });

									        var html = '';
									        html += '<tr class="barisNegara">';
									        html += '<td class="with-form-control"><select name="s_wisman[]" class="form-control s_wisman" data-parsley-required="true"><option value="" style="z-index: 2150000000;">Pilih Negara</option></select><div class="valid-feedback">Ok!</div><div class="invalid-feedback"></div></td>';
									        html += '<td><input type="number" class="form-control" name="i_wisman[]" placeholder="Jumlah Pengunjung" data-parsley-required="true" /><div class="valid-feedback">Ok!</div><div class="invalid-feedback"></div></td>';
									        html += '<td><center><button type="button" name="remove" class="btn btn-danger btn-sm remove">Hapus</button></center></td></tr>';
									        $('#item_table').append(html);
									    });
									    $(document).on('click', '.remove', function(){
									    	$(this).closest('tr').remove();
									    });
									});
								</script>
							
							</div>
						</div>
					</form>
				</div>

				<div class="modal-footer">
					<button type="button" id="btnReset" onclick="page_reset()" class="btn btn-default btn-sm">Reset</button>
					<button type="button" id="btnSave" onclick="simpan()" class="btn btn-primary btn-sm">Simpan</button>
				</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modaldetilKunjungan" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modaldetilKunjunganTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="table-responsive mt-3">
						<table class="table table-striped" cellspacing="0" width="100%">
							<thead>
								<tr>
									<td>Bulan Laporan</td>
									<td>:</td>
									<td id="bulan_lapor"></td>
								</tr>
								<tr>
									<td>Nama Hotel</td>
									<td>:</td>
									<td id="nama_hotel"></td>
								</tr>
								<tr>
									<td>Alamat Hotel</td>
									<td>:</td>
									<td id="alamat_hotel"></td>
								</tr>
								<tr>
									<td>Rata-rata Lama Tinggal</td>
									<td>:</td>
									<td id="rata_tinggal"></td>
								</tr>
								<tr>
									<td>Jumlah Pengunjung Nusantara</td>
									<td>:</td>
									<td id="sum_wisnus"><b></b></td>
								</tr>
								<tr>
									<td>Jumlah Pengunjung Mancanegara</td>
									<td>:</td>
									<td id="tot_wisman"><b></b></td>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<table class="table table-striped" cellspacing="0" width="100%" id="tabel_wisman">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Negara</th>
									<th>Jumlah Pengunjung</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
		</div>
	</div>
</div>


