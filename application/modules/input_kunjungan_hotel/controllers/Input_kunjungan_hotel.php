<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/vendor/autoload.php';
// $cache = new MyCustomPsr16Implementation();
// \PhpOffice\PhpSpreadsheet\Settings::setCache($cache);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
class Input_kunjungan_hotel extends CI_Controller {
	var $tabelna = 'kunjungan_hotel';
	var $idna    = 'id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_in_hotel','in_hotel');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->mybreadcrumb->add('Kunjungan Hotel', base_url('in_hotel'));
		$this->mybreadcrumb->add('Input Data', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'input_kunjungan_hotel';
		$data['page']        = 'Kunjungan Hotel';
		$data['content']     = 'content_in_hotel';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function hotelList(){
		if ($this->input->is_ajax_request()) {
			// POST data
			$postData = $this->input->post();
			// Get data
			$data = $this->in_hotel->getHotels($postData);
			echo json_encode($data);
		} else {
			redirect("_404","refresh");
		}
	}
	public function get_data_negara(){
		if ($this->input->is_ajax_request()) {
        	$neg = $this->in_hotel->get_data_negara();
        	echo json_encode(array_values($neg));
		} else {
			redirect("_404","refresh");	
		}
	}
	public function get_data_hotel_kunjungan(){
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');
			$data['data_kunjungan'] = $this->in_hotel->data_kunjungan_by_id($id);
			$xxx = $this->in_hotel->data_detil_wisman($id);
			$i = 0;
			$xv = array();
				foreach ($xxx as $key) {
					if ($key->nama_negara != null) {
						$i++;
						$xv[]["datana"] = array(
							"no" 			=> $i,
							"nama_negara" 	=> $key->nama_negara,
							"jumlah_wisman"	=> $key->sum_wisman." Orang",
						);
					}
				}
			$data['detil_wisman'] = $xv;
			echo json_encode($data);
		} else {
			redirect("_404","refresh");
		}	
	}
	public function get_data_hotel_byuser(){
		$data = $this->in_hotel->data_hotel_byid();
		echo json_encode($data);
	}
	public function get_data(){
		if($this->input->is_ajax_request()){
			$list = $this->in_hotel->get_datatables();
			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $key) {
				$no++;
				$row    = array();
				$row[]  = $no.'.';
				$row[]  = $key->nama;
				$tgls 	= explode("-", $key->create_at);
				$bulan	= date('m', strtotime($key->create_at));
				// $tgls[1];
				switch($bulan) {
					case"01"; $bulan="Januari"; break;
					case"02"; $bulan="Februari"; break;
					case"03"; $bulan="Maret"; break;
					case"04"; $bulan="April"; break;
					case"05"; $bulan="Mei"; break;
					case"06"; $bulan="Juni"; break;
					case"07"; $bulan="Juli"; break;
					case"08"; $bulan="Agustus"; break;
					case"09"; $bulan="September"; break;
					case"10"; $bulan="Oktober"; break;
					case"11"; $bulan="November"; break;
					case"12"; $bulan="Desember"; break;
				}
				$row[]  = $bulan." ".date('Y', strtotime($key->create_at));
				$row[]  = $this->in_hotel->TotalPengunjung($key->id)." Orang";
				$row[]  = '<center>
				<button type="button" onclick="detail_data(\''.$key->id.'\')" class="btn btn-default btn-icon btn-sm" data-toggle="modal" data-target="#modaldetilKunjungan" title="Detail Data "><i class="fa fa-share-square"></i></button>
				<button type="button" onclick="hapus_data(\''.$key->id.'\',\'KUNJUNGAN HOTEL \')" class="btn btn-danger btn-icon btn-sm" title="Hapus Hotel "><i class="fa fa-times"></i></button></center>
				';
				// Tombol Edit
				// <button type="button" onclick="edit_data(\''.$key->id.'\')" class="btn btn-info btn-icon btn-sm" data-toggle="modal" data-target="#modalKunjungan" title="Edit Hotel '.ucwords($this->in_hotel->namaHotels($key->hotel_id)).'"><i class="fa fa-pencil-alt"></i></button>

				$data[] = $row;
			}
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->in_hotel->count_all(),
				"recordsFiltered" => $this->in_hotel->count_filtered(),
				"data" => $data,
			);
			echo json_encode($output);
		}else{
			redirect("_404","refresh");
		}
	}
	public function get_data_by_id(){
		if($this->input->is_ajax_request()){
			$id   = $this->input->post('id');
			$data = $this->in_hotel->get_by_id($this->tabelna,'kunjungan_hotel.id',$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _CekLastID(){
		$dariDB = $this->in_hotel->CekLastId();
		$nourut = substr($dariDB, 4, 3);
		$idbaru = $nourut + 1;
		$idnew	= sprintf("%03s", $idbaru);
		return $idnew;
	}
	public function process_simpan(){
		if($this->input->is_ajax_request()){
			$this->_validate();
			$user_id = $this->session->userdata('user_id');
			date_default_timezone_set("Asia/Bangkok");
			$nganu = 'VST-'.$this->_CekLastID();
			$cekk = $this->input->post('checkmanca');
			if ($cekk != '1') {
				$data2 = array(
					'id'				=> $nganu,
					'hotel_id'      	=> htmlspecialchars($this->input->post('hotel_id')),
					'sum_wisnus'		=> htmlspecialchars($this->input->post('wisnus')),
					'ratarata_menginap' => $this->input->post('rata_tinggal'),
					'create_at'			=> date('Y-m-d',strtotime($this->input->post('date'))),
					'last_update' 		=> date('Y-m-d H:i:s'),
					'user_id'     		=> $user_id,
					'status'			=> 1
				);
				$data3 = array(
					'negara_id'		=> null,
			        'sum_wisman'	=> 0,
			        'kunjungan_id'	=> $nganu
				);
				$query  = $this->in_hotel->save($this->tabelna,$data2);
				$query1 = $this->in_hotel->save('detail_wisman',$data3);
				if($query && $query1 > 0){
					$response['status'] = true;
					$response['notif']  = 'Data berhasil disimpan!';
				}else{
					$response['status'] = false;
					$response['notif']  = 'Data gagal disimpan!';
				}
				echo json_encode($response);

			} else {

				$this->db->trans_start();		
					$data1 = array(
						'id'				=> $nganu,
						'hotel_id'      	=> htmlspecialchars($this->input->post('hotel_id')),
						'sum_wisnus'		=> htmlspecialchars($this->input->post('wisnus')),
						'ratarata_menginap' => $this->input->post('rata_tinggal'),
						'create_at'			=> date('Y-m-d',strtotime($this->input->post('date'))),
						'last_update' 		=> date('Y-m-d H:i:s'),
						'user_id'     		=> $user_id,
						'status'			=> 1
					);
					$this->db->insert('kunjungan_hotel', $data1);

					$contries = $this->input->post('s_wisman'); // Ambil data nis dan masukkan ke variabel nis
				    $s_wisman = $this->input->post('i_wisman'); // Ambil data nama dan masukkan ke variabel nama
				    $data = array();
				    
				    $index = 0; // Set index array awal dengan 0
				    foreach($contries as $datacontry){ // Kita buat perulangan berdasarkan nis sampai data terakhir
				      array_push($data, array(
				        'negara_id'		=> $datacontry,
				        'sum_wisman'	=> $s_wisman[$index],  // Ambil dan set data nama sesuai index array dari $index
				        'kunjungan_id'	=> $nganu
				      ));
				      $index++;
				    }
				    $this->db->insert_batch('detail_wisman', $data);
			    $this->db->trans_complete();
			    if ($this->db->trans_status() === FALSE) {
			    	$response['status'] = false;
					$response['notif']  = 'Data gagal disimpan!';
				} else {
					$response['status'] = true;
					$response['notif']  = 'Data berhasil disimpan!';
				}
				echo json_encode($response);
			}
		}else{
			redirect("_404",'refresh');
		}
	}
	// public function process_update(){
	// 	if($this->input->is_ajax_request()){
	// 		$this->_validate();
	// 		$id      = $this->input->post('id');
	// 		$user_id = $this->session->userdata('user_id');
	// 		$data = array(
	// 			'nama'       	=> htmlspecialchars($this->input->post('nama_hotel')),
	// 			'kategori_id'	=> htmlspecialchars($this->input->post('kategori_hotel')),
	// 			'alamat'		=> htmlspecialchars($this->input->post('alamat_hotel')),
	// 			'last_update' 	=> date('Y-m-d H:i:s'),
	// 			'user_id'     	=> $user_id
	// 		);
	// 		$query = $this->in_hotel->update('hotel', $data, array('id' => $id));
	// 		if($query > 0){
	// 			$response['status'] = true;
	// 			$response['notif']  = 'Data berhasil diperbaharui!';
	// 		}else{
	// 			$response['status'] = false;
	// 			$response['notif']  = 'Data gagal diperbaharui!';
	// 		}
	// 		echo json_encode($response);
	// 	}else{
	// 		redirect("_404",'refresh');
	// 	}
	// }
	private function _validate(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		
		$this->form_validation->set_rules('pilih_hotel', 'Nama Hotel', 'trim|required');
		$this->form_validation->set_rules('date', 'Tanggal Laporan', 'required');
		$this->form_validation->set_rules('rata_tinggal', 'Rata-rata Tinggal', 'required');
		$this->form_validation->set_rules('wisnus', 'Jumlah Wisatawan Nusantara', 'required');

		if ($this->form_validation->run() == FALSE) {
			
			$ni = ['pilih_hotel', 'date', 'rata_tinggal', 'wisnus'];
			
			for ($i=0; $i < count($ni) ; $i++) { 
				$inputan = form_error($ni[$i]);
				if ($inputan !='') {
					$response['inputerror'][]   = $ni[$i];
					$response['error_string'][] = $inputan;
					$response['status']         = 'invalid';
				}
			}
		}
		if($response['status'] === 'invalid') {
			echo json_encode($response);
			exit();
		}
	}
	public function status_aktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 1,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->in_hotel->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function status_nonaktif(){
		if($this->input->is_ajax_request()){
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'status'      => 0,
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->in_hotel->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_hapus(){
		if($this->input->is_ajax_request()){
			$id    = $this->input->post('id');
			$query = $this->in_hotel->delete_by_id($this->tabelna,$this->idna,$id);
			$this->in_hotel->delete_by_id('detail_wisman','kunjungan_id',$id);
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil dihapus!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal dihapus!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
}
