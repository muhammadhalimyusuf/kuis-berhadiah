<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_in_hotel extends CI_Model {
	var $table 			= 'kunjungan_hotel';
	var $column_order 	= array(null,'hotel_id','create_at','sum_wisnus',null); 
	var $column_search 	= array('nama');
	var $order = array('create_at' => 'desc');
	private $_batchImport;

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	function getHotels($postData){
		$response = array();
		if(isset($postData['search']) ){
		// Select record
			$this->db->select('*');
			$this->db->where("nama like '%".$postData['search']."%' ");
			$records = $this->db->get('hotel')->result();
			foreach($records as $row ){
				$response[] = array("value"=>$row->id,"label"=>$row->nama);
			}
		}
		return $response;
	}
	public function TotalPengunjung($id){
		$query = $this->db->query("SELECT (SELECT SUM(IF(id = '$id', sum_wisnus, 0))FROM kunjungan_hotel)+(SELECT SUM(IF(kunjungan_id = '$id', sum_wisman, 0))FROM detail_wisman) AS Total");
		$hasil = $query->row();
		return $hasil->Total;
	}
	public function get_data_negara(){
        $query = $this->db->query('SELECT id, negara FROM ref_negara WHERE status="1" ORDER BY negara ASC');
        return $dropdowns = $query->result();
    }
	private function _get_datatables_query(){
		$akses = $this->session->userdata('id_akses');
		if ($akses == '4') {
			$where = $this->data_hotel_byuser();
			$this->db->select('kunjungan_hotel.id');
			$this->db->select('kunjungan_hotel.create_at');
			$this->db->select('hotel.nama');
			$this->db->from('kunjungan_hotel');
			$this->db->join('hotel', 'kunjungan_hotel.hotel_id = hotel.id', 'left');
			$this->db->where('kunjungan_hotel.hotel_id', $where);
		} else {
			$this->db->select('kunjungan_hotel.id');
			$this->db->select('kunjungan_hotel.create_at');
			$this->db->select('hotel.nama');
			$this->db->from('kunjungan_hotel');
			$this->db->join('hotel', 'kunjungan_hotel.hotel_id = hotel.id', 'left');
		}
		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0) {
					$this->db->group_start();
					$this->db->like('lower('.$item.')', strtolower($_POST['search']['value']));
				}
				else{
					$this->db->or_like('lower('.$item.')', strtolower($_POST['search']['value']));
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		if(isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	public function get_datatables(){
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	public function count_filtered(){
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function count_all(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	public function save($table, $data){
		$this->db->insert($table, $data);
		return $this->db->affected_rows();
	}
	public function save_batch($table, $data){
    	return $this->db->insert_batch($table, $data);
    }
	public function get_by_id($table,$where,$id){
		$this->db->select('kunjungan_hotel.id,kunjungan_hotel.hotel_id,hotel.nama,kunjungan_hotel.sum_wisnus');
		// $this->db->select('*');
		$this->db->from($table);
		$this->db->where($where,$id);
		$this->db->join('hotel', 'hotel.id = kunjungan_hotel.hotel_id');
		$this->db->join('detail_wisman', 'detail_wisman.kunjungan_id = kunjungan_hotel.id', 'left');
		$query = $this->db->get();
		return $query->row();
	}
	public function update($table, $data, $where){
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}
	public function delete_by_id($table,$where,$id){
		$this->db->where($where, $id);
		$this->db->delete($table);
		return $this->db->affected_rows();
	}
	public function get_kategori(){
		$query = $this->db->get('ref_kategori');
		return $query->result();
	}
	public function CekLastID(){
        $query = $this->db->query("SELECT MAX(id) as lastid from kunjungan_hotel");
        $hasil = $query->row();
        return $hasil->lastid;
    }
    public function data_kunjungan(){
    	// $this->db->select('kunjungan_hotel.id AS id_kunjungan');
    	// $this->db->select('kunjungan_hotel.hotel_id');
    	// $this->db->select('kunjungan_hotel.sum_wisnus');
    	// $this->db->select('kunjungan_hotel.create_at AS tgl_input_kunjungan');
    	// $this->db->select('kunjungan_hotel.last_update AS tgl_update_kunjungan');
    	// $this->db->select('kunjungan_hotel.user_id AS penginput_kunjungan');
    	// $this->db->select('kunjungan_hotel.status AS status_kunjungan');
    	// $this->db->select('detail_wisman.id AS id_detail_wisman');
    	// $this->db->select('detail_wisman.negara_id');
    	// $this->db->select('ref_negara.negara AS nama_negara');
    	// $this->db->select('detail_wisman.sum_wisman');
    	// $this->db->select('hotel.id AS id_hotel');
    	// $this->db->select('hotel.nama AS nama_hotel');
    	// $this->db->select('hotel.alamat AS alamat_hotel');
    	// $this->db->select('ref_kategori.kategori AS kategori_hotel');
    	// $this->db->select('ref_negara.id AS id_negara');
    	// $this->db->select('ref_kategori.id AS id_kategori');
    	$this->db->from('kunjungan_hotel');
    	$this->db->join('detail_wisman', 'kunjungan_hotel.id = detail_wisman.kunjungan_id', 'left');
    	$this->db->join('hotel', 'kunjungan_hotel.hotel_id = hotel.id', 'left');
    	$this->db->join('ref_negara', 'detail_wisman.negara_id = ref_negara.id', 'left');
    	$this->db->join('ref_kategori', 'hotel.kategori_id = ref_kategori.id', 'left');
    }
    public function data_kunjungan_by_id($id){
    	$this->db->select('hotel.nama AS nama_hotel');
    	$this->db->select('hotel.alamat AS alamat_hotel');
    	$this->db->select('kunjungan_hotel.sum_wisnus');
    	$this->db->select('kunjungan_hotel.create_at');
    	$this->db->select('kunjungan_hotel.ratarata_menginap');
    	$this->db->select('SUM(detail_wisman.sum_wisman) AS total_wisman');
    	$this->data_kunjungan();
    	$this->db->where('kunjungan_hotel.id', $id);
    	$query = $this->db->get();
    	return $query->row();
    }
    public function data_detil_wisman($id){
    	$this->db->select('ref_negara.negara AS nama_negara');
    	$this->db->select('detail_wisman.sum_wisman');
    	$this->data_kunjungan();
    	$this->db->where('kunjungan_hotel.id', $id);
    	$query = $this->db->get();    		
    	return $query->result();
    }
    public function data_hotel_byuser(){
    	$user_id 	= $this->session->userdata('user_id');
		$tabel 		= $this->session->userdata('tipe_user');
    	$query = $this->db
    		->select($tabel.'.hotel_id')
    		->from($tabel)
    		->where($tabel.'.id',$user_id)
    		->get();
    	return $query->row()->hotel_id;
    }
   
    public function data_hotel_byid(){
    	$where = $this->data_hotel_byuser();
    	$query = $this->db
    		->select('hotel.id')
    		->select('hotel.nama')
    		->from('hotel')
    		->where('hotel.id', $where)
    		->get();
    	return $query->row();
    }
}
