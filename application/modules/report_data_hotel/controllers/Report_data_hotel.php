<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report_data_hotel extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_report_data_hotel','modelna');
		$this->db1 = $this->load->database('default', TRUE);
	}
	private function _bulan($bln)
	{
		switch($bln) {
			case"01"; $bln="Januari"; break;
			case"02"; $bln="Februari"; break;
			case"03"; $bln="Maret"; break;
			case"04"; $bln="April"; break;
			case"05"; $bln="Mei"; break;
			case"06"; $bln="Juni"; break;
			case"07"; $bln="Juli"; break;
			case"08"; $bln="Agustus"; break;
			case"09"; $bln="September"; break;
			case"10"; $bln="Oktober"; break;
			case"11"; $bln="November"; break;
			case"12"; $bln="Desember"; break;
		}
		return $bln;
	}
	public function index(){
		$this->mybreadcrumb->add('Laporan', base_url('report_data_hotel'));
		$this->mybreadcrumb->add('Data Hotel', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'report_data_hotel';
		$data['page']        = 'Laporan Data Hotel';
		$data['content']     = 'content_report_data_hotel';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function getDataKategori()
	{
		if ($this->input->is_ajax_request()) {
			$selectna = array('id', 'kategori');
			$data = $this->modelna->refensiData($selectna, 'ref_kategori');
			$data1[] = array('id' => '', 'kategori' => '');
			$gab = array_merge($data1, $data);
			echo json_encode($gab);
		} else {
			redirect('_404','refresh');
		}
	}
	public function pdf()
	{
		$this->load->helper('pdf_helper');
		require_once(APPPATH.'tcpdf/tcpdf.php');
		
		$status   = $this->input->post('report_dh_status');
		$kategori = $this->input->post('report_dh_kategori');
		$date 	  = $this->input->post('date');

		if ($status != "") {
			if ($kategori) {
				// data status + kategori
				$where = ['hotel.status' => $status, 'hotel.kategori_id' => $kategori];
			} else {
				// data status
				$where = ['hotel.status' => $status];
			}

			$data = $this->modelna->dataHotel($where);

			// create new PDF document
			$pdf = new TCPDF('P', 'mm', [330,215]);

			$pdf->SetAuthor('disporabudpar.tsm');
			$pdf->SetTitle('Laporan Data Hotel '.date('d M Y'));
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetMargins(10, 15, 10);
			$pdf->SetAutoPageBreak(TRUE, 15);

			// add a page
			$pdf->AddPage();

			$pdf->Image(K_PATH_IMAGES.'logokota.jpg', '15', '', 30, 0, 'JPG', '', 'C', false, 72, '', false, false, 0, false, false, false);
			$pdf->SetFont('helvetica','','14pt');
			$pdf->Cell(35, 0, '', 0, 0, 'C', 0, '', 0);
			$pdf->Cell(160, 0, 'PEMERINTAH KOTA TASIKMALAYA', 0, 1, 'C', 0, '', 0);
			
			$pdf->SetFont('helvetica','B','18pt');
			$pdf->Cell(35, 0, '', 0, 0, 'C', 0, '', 0);
			$pdf->Cell(160, 0, 'DINAS KEPEMUDAAN OLAHRAGA, KEBUDAYAAN', 0, 1, 'C', 0, '', 0);
			$pdf->Cell(35, 0, '', 0, 0, 'C', 0, '', 0);
			$pdf->Cell(160, 0, 'DAN PARIWISATA', 0, 1, 'C', 0, '', 0);

			$pdf->SetFont('helvetica','','12pt');
			$pdf->Cell(35, 0, '', 0, 0, 'C', 0, '', 0);
			$pdf->Cell(160, 0, 'Jalan Dadaha, Kahuripan, Kecamatan Tawang Telp/Fax. (0265) 314376', 0, 1, 'C', 0, '', 0);

			$pdf->SetFont('helvetica','','12pt');
			$pdf->Cell(35, 7, '', 0, 0, 'C', 0, '', 0);
			$pdf->Cell(160, 7, 'Kode Pos 46124', 0, 1, 'R', 0, '', 0);

			$py = $pdf->getY();
			$pdf->Image(K_PATH_IMAGES.'garisP.jpg', '', $py, 195, 0, 'JPG', '', 'C', false, 75, '', false, false, 0, false, false, false);

			$pdf->ln();
			$text1 = 'DATA HOTEL KOTA TASIKMALAYA TAHUN '.date('Y');
			$pdf->SetFont('helvetica','B',14);
			$pdf->Cell(0, 8, $text1, 0, 1, 'C', 0, '', 3);

			$head = array('NO', 'KATEGORI', 'NAMA HOTEL', 'ALAMAT');
			$head_row = count($head);
			$w 	  = array(15, 35, 45, 100);
			$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
			$pdf->SetFillColor(229, 231, 233);
        	$pdf->SetTextColor(0);
			for ($i=0; $i < $head_row; $i++) { 
				$pdf->Cell($w[$i], 7, $head[$i], 1, 0, 'C', 1);
			}$pdf->ln();
			$head1 = array('1', '2', '3', '4');
			$pdf->SetFont('helvetica','I',12);
			for ($i=0; $i < $head_row; $i++) { 
				$pdf->Cell($w[$i], 0, $head1[$i], 1, 0, 'C', 1);
			}$pdf->ln();

			$no=1;
			$tbl = '
				<table cellpadding="1mm" border="0.2">';
	        foreach ($data as $d) 
	        {
	        $tbl.=	'
	        	<tr nobr="true">
	    			<td width="15mm" align="center">'.$no++.'.</td>
					<td width="35mm" align="center">'.strtoupper($d['kategori_hotel']).'</td>
	    			<td width="45mm">'.$d['nama_hotel'].'</td>
					<td width="100mm">'.$d['alamat_hotel'].'</td>
	            </tr>';
	        }
	        $tbl.='</tbody></table>';
			$pdf->SetFont('helvetica', '', 12);
			$pdf->writeHTML($tbl);

			
			if ($date != "") {
				$dt 	= date('d', strtotime($date));
				$yr 	= date('Y', strtotime($date));
				$bln 	= date('m', strtotime($date));
			} else {
				$dt 	= date('d');
				$yr 	= date('Y');
				$bln 	= date('m');
			}
			
			$bln	= $this->_bulan($bln);
			$ttgl  = $dt.' '.$bln.' '.$yr;
			$kadis = $this->modelna->ttd('Kepala Dinas');
			$create= $this->modelna->ttd('Kepala Bidang Pariwisata');
			$ttd = '
				<table width="100%">
					<tr nobr="true">
						<td align="center" width="50%">
							<p>
								Mengetahui,<br>
								'.$kadis['jabatan'].' Kepemudaan Olahraga,
								<br/>Kebudayaan dan Pariwisata Kota Tasikmalaya
								<br/><br/><br/><br/><br/>
								<b>'.strtoupper($kadis['nama']).'</b><br>
								NIP. '.$kadis['nip'].'
							</p>
						</td>
						<td align="center" width="50%">
							<p>
								Tasikmalaya, '.$ttgl.'<br>
								'.$create['jabatan'].'
								<br/><br/><br/><br/><br/><br/>
								<b>'.strtoupper($create['nama']).'</b><br>
								NIP. '.$create['nip'].'
							</p>
						</td>
					</tr>
				</table>
			';

			$pdf->SetFont('helvetica', '', 12);
			$pdf->writeHTML($ttd);
	        
			// -----------------------------------------------------------------------------
			//Close and output PDF document
			$pdf->Output('Laporan Data Hotel '.$ttgl.'.pdf', 'I');
			//============================================================+
			// END OF FILE
			//============================================================+
		} else {
			redirect('_404','refresh');
		}
		

	}
		
}
