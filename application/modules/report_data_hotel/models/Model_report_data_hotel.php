<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_report_data_hotel extends CI_Model {
	var $tabel_a	= 'hotel';
	var $tabel_b	= 'ref_kategori';
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function refensiData($selectna, $tabelna)
	{
		$this->db->select($selectna);
		return $this->db->get($tabelna)->result_array();
	}
	public function dataHotel($where)
	{
		$this->db->select(''.$this->tabel_a.'.id AS id_hotel');
		$this->db->select(''.$this->tabel_b.'.kategori AS kategori_hotel');
		$this->db->select(''.$this->tabel_a.'.nama AS nama_hotel');
		$this->db->select(''.$this->tabel_a.'.alamat AS alamat_hotel');
		$this->db->from($this->tabel_a);
		$this->db->join($this->tabel_b, ''.$this->tabel_b.'.id = '.$this->tabel_a.'.kategori_id', 'left');
		$this->db->where($where);
		$this->db->order_by('hotel.kategori_id ASC, nama_hotel ASC');
		return $this->db->get()->result_array();
	}
	public function ttd($jabatan)
	{
		$this->db->select('pegawai.nip');
		$this->db->select('pegawai.nama');
		$this->db->select('ref_jabatan.jabatan');
		$this->db->from('pegawai');
		$this->db->join('ref_jabatan', 'ref_jabatan.id = pegawai.jabatan_id', 'left');
		$this->db->where('jabatan', $jabatan);
		$query = $this->db->get();
		return $query->row_array();
	}
}
