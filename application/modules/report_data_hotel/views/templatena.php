<table border="1" cellspacing="0" cellpadding="0">
    <tr>
        <td width="15%">1</td>
        <td width="85%" bgcolor="#cccccc" align="center">
        	<span style="font-size: 10pt;">PEMERINTAH KOTA TASIKMALAYA</span><br>
        	<span style="font-size: 12pt;">DINAS KEPEMUDAAN, OLAHRAGA, KEBUDAYAAN DAN PARIWISATA</span><br>
        	<span style="font-size: 9pt;">Jalan Dadaha, Kahuripan, Kecamatan Tawang Telp/Fax. (0265) 314376</span>
    	</td>
    </tr>
</table>
<style>
	#halamanlaporan{
		font-family: "Bookman Old Style", "Verdana";
		font-size: 0.35cm;
		width: 600px;
		margin: auto;
	}
	#logo{
		width: 100%;
		height: 2.5cm;
		background: url(http://www.esakip.tasikmalayakota.go.id/images/logokotakab-dark.png) no-repeat;
		background-size: 2.5cm 2cm;
		background-position: center;
	}
	.isilaporan{
		font-size: 0.35cm;
		line-height: 1.3;
	}
	.isilaporan h3{
		font-size: 0.35cm;
		font-weight: normal;
		text-align: center;
	}
	.isilaporan h4{
		font-size: 0.35cm;
		font-weight: bold;
		text-align: center;
	}
	.isilaporan h5{
		font-size: 0.35cm;
		font-weight: bold;
		text-align: center;
		margin-top: 20px;
		margin-bottom: 20px;
	}
	.isilaporan p{
		text-align: justify;
	}
	.tblPihak{
		width: 100%;
		font-size: 0.35cm;
	}
	.tblPihak td{
		width: 50%;
		text-align: center;
	}
	.tbdata{
		width: 100%;
		font-size: 0.35cm;
		margin-top: 20px;
		border-collapse: collapse;
	}
	.tbdata th{
		padding: 5px;
		text-align: center;
	}
	.tbdata td{
		padding-left: 3px;
		padding-right: 3px;
		vertical-align: top;
	}
	.tengah{
		text-align: center;
	}
	.kanan{
		text-align: right;
	}
	img { display:block }
</style>
<table id="halamanlaporan" >
	<tbody>
		<tr>
			<td>
				<div class="isilaporan">
					<table>
						<tr>
							<td>
								<img src="http://www.esakip.tasikmalayakota.go.id/images/logokotakab-dark.png">
							</td>
							<td>
								<h3>PEMERINTAH KOTA TASIKMALAYA</h3>
								<h4>DINAS KEPEMUDAAN, OLAHRAGA, KEBUDAYAAN DAN PARIWISATA</h4>
								<h3>Jalan Dadaha, Kahuripan, Kecamatan Tawang Telp/Fax. (0265) 314376</h3>
							</td>
						</tr>
					</table>										
				</div>
				<hr>
			</td>
		</tr>
		<tr>
			<td>
			<div class="isilaporan">
			<h5>DATA HOTEL KOTA TASIKMALAYA</h5>
			<table class="tbdata" border="1">
				<thead>
					<tr>
						<th width="5%">NO</th>
						<th width="20%">NAMA HOTEL</th>
						<th width="55%">ALAMAT</th>
						<th width="20%">KATEGORI</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="tengah">1</td>
						<td class="tengah">2</td>
						<td class="tengah">3</td>
						<td class="tengah">4</td>
					</tr>
					<tr>
						<td class="tengah">1</td>
						<td>ABADI</td>
						<td>JALAN RAYA MANA WAE</td>
						<td class="tengah">Non Bintang</td>
					</tr>
				</tbody>
			</table>
			</div>
			</td>
		</tr>
	</tbody>
</table>

