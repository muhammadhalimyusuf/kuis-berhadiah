<link href="<?php echo base_url(); ?>assets/app/plugin/datatables/datatables.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/app/plugin/datatables/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/set_akun.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	
	<div class="row">
				<!-- begin col-6 -->
				<div class="col-xl-12">
					<!-- begin nav-pills -->
					<ul class="nav nav-pills mb-2">
						<li class="nav-item">
							<a href="#nav-pills-tab-1" data-toggle="tab" class="nav-link active">
								<span class="d-sm-none">Super Admin</span>
								<span class="d-sm-block d-none">SUPER ADMIN</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="#nav-pills-tab-2" data-toggle="tab" class="nav-link">
								<span class="d-sm-none">Admin</span>
								<span class="d-sm-block d-none">ADMIN</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="#nav-pills-tab-3" data-toggle="tab" class="nav-link">
								<span class="d-sm-none">User</span>
								<span class="d-sm-block d-none">USER</span>
							</a>
						</li>
					</ul>
					<!-- end nav-pills -->
					<!-- begin tab-content -->
					<div class="tab-content p-15 rounded bg-white mb-4">
						<!-- begin tab-pane -->
						<div class="tab-pane fade active show" id="nav-pills-tab-1">
							<h5 class="m-t-10">List <?php echo ucwords($page); ?> Super Admin</h3>
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight">
									<button type="button" class="btn btn-info btn-sm" onclick="tambah_data()" data-toggle="modal" data-target="#modalHotel">Tambah</button>
								</div>
							</div>
							<div class="table-responsive mt-3">
								<table id="list_data" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th><center>No</center></th>
											<th>Foto</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th><center>Action</center></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<!-- end tab-pane -->
						<!-- begin tab-pane -->
						<div class="tab-pane fade" id="nav-pills-tab-2">
							<h3 class="m-t-10">Nav Pills Tab 2</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor, 
								est diam sagittis orci, a ornare nisi quam elementum tortor. 
								Proin interdum ante porta est convallis dapibus dictum in nibh. 
								Aenean quis massa congue metus mollis fermentum eget et tellus. 
								Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien, 
								nec eleifend orci eros id lectus.
							</p>
						</div>
						<!-- end tab-pane -->
						<!-- begin tab-pane -->
						<div class="tab-pane fade" id="nav-pills-tab-3">
							<h3 class="m-t-10">Nav Pills Tab 3</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Integer ac dui eu felis hendrerit lobortis. Phasellus elementum, nibh eget adipiscing porttitor, 
								est diam sagittis orci, a ornare nisi quam elementum tortor. 
								Proin interdum ante porta est convallis dapibus dictum in nibh. 
								Aenean quis massa congue metus mollis fermentum eget et tellus. 
								Aenean tincidunt, mauris ut dignissim lacinia, nisi urna consectetur sapien, 
								nec eleifend orci eros id lectus.
							</p>
						</div>
						<!-- end tab-pane -->
					</div>
					<!-- end tab-content -->
				</div>
				<!-- end col-6 -->
			</div>

</div>
