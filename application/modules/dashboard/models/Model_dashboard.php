<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_dashboard extends CI_Model {
    public function get_kalender(){
		$this->db->select('kalender_akademik.id');
		$this->db->select('kalender_akademik.kegiatan');
		$this->db->select('kalender_akademik.tgl_start');
		$this->db->select('kalender_akademik.tgl_end');
		$this->db->select('kalender_status.warna');
		$this->db->from('kalender_akademik');
		$this->db->join('kalender_status','kalender_akademik.status_id=kalender_status.id');
		$query = $this->db->get();
		return $query->result();
	}
	public function hitungHotel($status){
		$this->db->where('status', $status);
		$query = $this->db->get('hotel');
		    if($query->num_rows()>0) {
				return $query->num_rows();
		    } else {
				return 0;
		    }
    }
    public function hitungUser(){
    	$this->db->where('status', '1');
    	$query0 = $this->db->get('developer');
    	$query  = $this->db->get('admin');
    	$query1 = $this->db->get('user');
    	$a = $query->num_rows()+$query1->num_rows()+$query0->num_rows();
    		if ($a > 0) {
    			return $a;
    		} else {
    			return 0;
    		}	
    }
}
