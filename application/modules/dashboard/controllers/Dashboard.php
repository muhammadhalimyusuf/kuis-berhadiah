<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->app->role_akses();
		$this->load->model('Model_dashboard','dashboard');
		$this->db1 = $this->load->database('default', TRUE);
		date_default_timezone_set('Asia/Jakarta');
	}
	public function index(){
		$this->mybreadcrumb->add('Dashboard', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'dashboard';
		$data['page']        = 'Dashboard';
		$data['content']     = 'content_dashboard';
		$data['hotel_aktif'] = $this->dashboard->hitungHotel('1');
		$data['hotel_off'] 	 = $this->dashboard->hitungHotel('0');
		$data['hit_user']	 = $this->dashboard->hitungUser();
		$this->load->view('dashboard_view',$data);
	}
}
