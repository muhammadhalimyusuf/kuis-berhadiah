<div id="header" class="header navbar-default">
	<div class="navbar-header">
		<a href="<?php echo base_url(); ?>dashboard" class="navbar-brand"> <img src="<?php echo base_url(); ?>assets/app/images/logo/<?php echo $this->session->userdata('app_logo_primary'); ?>" alt="" /> <b><?php echo $this->session->userdata('app_name'); ?></b> </a>
		<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<ul class="navbar-nav navbar-right">
		<li class="dropdown navbar-user">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<img src="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" alt="" /> 
				<span class="d-none d-md-inline"><?php echo ucwords($this->session->userdata('pro_nama')); ?></span> <b class="caret"></b>
			</a>
			<div class="dropdown-menu dropdown-menu-right">
				<a href="<?php echo base_url(); ?>personal_info" class="dropdown-item">Personal Info</a>
				<div class="dropdown-divider"></div>
				<a href="javascript:logout('<?php echo ucwords($this->session->userdata('pro_nama')); ?>')" class="dropdown-item">Log Out</a>
			</div>
		</li>
	</ul>
</div>