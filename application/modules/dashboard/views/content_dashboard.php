<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>

	<div class="row">
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-red">
				<div class="stats-icon"><i class="fa fa-building"></i></div>
				<div class="stats-info">
					<h4>HOTEL NON AKTIF</h4>
					<p><?php echo $hotel_off; ?> Hotel</p>	
				</div>
				<?php 
				$a = $this->session->userdata('id_akses');
				if ($a == '1' or $a == '2'): ?>
					<div class="stats-link">
						<a href="<?php echo base_url(); ?>md_hotel">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
					</div>					
				<?php else: ?>
					<div class="stats-link">
						<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
					</div>
				<?php endif ?>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-blue">
				<div class="stats-icon"><i class="fa fa-building"></i></div>
				<div class="stats-info">
					<h4>HOTEL AKTIF</h4>
					<p><?php echo $hotel_aktif; ?> Hotel</p>	
				</div>
				<?php 
				$a = $this->session->userdata('id_akses');
				if ($a != '4'): ?>
					<div class="stats-link">
						<a href="<?php echo base_url(); ?>md_hotel">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
					</div>					
				<?php else: ?>
					<div class="stats-link">
						<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
					</div>
				<?php endif ?>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-grey-darker">
				<div class="stats-icon"><i class="fa fa-users"></i></div>
				<div class="stats-info">
					<h4>USER AKTIF</h4>
					<p><?php echo $hit_user; ?> User</p>	
				</div>
				<div class="stats-link">
					<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
		<!-- begin col-3 -->
		<div class="col-lg-3 col-md-6">
			<div class="widget widget-stats bg-black-lighter">
				<div class="stats-icon"><i class="fa fa-clock"></i></div>
				<div class="stats-info">
					<h4>HARI INI</h4>
					<p><?= date('d M'); ?></p>
				</div>
				<div class="stats-link">
					<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
		</div>
		<!-- end col-3 -->
	</div>

	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title"><?php echo ucwords($page); ?></h4>
		</div>
		<div class="panel-body">
			
		<div id="viewDiv"></div>					
		</div>
	</div>
	<!-- end panel -->
</div>
		
