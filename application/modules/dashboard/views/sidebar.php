<div id="sidebar" class="sidebar">
	<div data-scrollbar="true" data-height="100%">
		<ul class="nav active">
			<li class="nav-profile">
				<a href="javascript:;" data-toggle="nav-profile">
					<div class="cover with-shadow"></div>
					<div class="image">
						<img style="height: 34px;width: 34px" src="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" alt="Foto Profil" />
					</div>
					<div class="info">
						<b class="caret pull-right"></b>
						<?php echo ucwords($this->session->userdata('pro_nama')); ?>
						<small><?php echo ucwords($this->session->userdata('pro_nama_akses')); ?></small>
					</div>
				</a>
			</li>
			<li>
				<ul class="nav nav-profile">
					<li <?php if ('personal_info' == $link) { echo "class='active'"; }else{ echo ""; } ?>><a href="<?php echo base_url(); ?>personal_info"><i class="fa fa-address-card"></i>Personal Info</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav">
			<?php 
			$akses = $this->session->userdata('menu_id');
			$group_menu = $this->db->query("SELECT * FROM app_menu WHERE id_menu in ($akses) AND level = 1 AND status = 1 ORDER BY urutan ASC")->result();
			if (count($group_menu)>0) {
				foreach ($group_menu as $level1) {
					?>
					<li class="nav-header"><?php echo $level1->nama_menu; ?></li>
					<?php
					$menu = $this->db->query("SELECT * FROM `app_menu` WHERE id_menu in ($akses) AND `parent` = '$level1->id_menu' AND `level` = 2 AND `status` = 1 ORDER BY `urutan` ASC")->result();
					if (count($menu)) {
						foreach ($menu as $level2) {
							if ($level2->anak == 0) {
								?>
								<li <?php if ($level2->link == $link) { echo "class='active'"; }else{ echo ""; } ?>>
									<a href="<?php echo base_url().$level2->link; ?>"><i class="<?php echo $level2->icon; ?>"></i> <span><?php echo $level2->nama_menu; ?></span></a>
								</li>
								<?php
							}else{
								?>
								<li class="has-sub">
									<a href="javascript:;">
										<b class="caret"></b>
										<i class="<?php echo $level2->icon; ?>"></i>
										<span><?php echo $level2->nama_menu; ?></span>
									</a>
									<ul class="sub-menu">
										<?php 
										$menu_sub  = $this->db->query("SELECT * FROM `app_menu` WHERE id_menu in ($akses) AND `parent` = '$level2->id_menu' AND `level` = 3 AND `status` = 1 ORDER BY `urutan` ASC")->result();
										if (count($menu_sub)>0) {
											foreach ($menu_sub as $level3) {
												?>
												<li <?php if ($level3->link == $link) { echo "class='active'"; }else{ echo ""; } ?>>
													<a href="<?php echo base_url().$level3->link;?>"><?php echo $level3->nama_menu; ?></a>
												</li>
												<?php
											}
										}else{
											echo "";
										}
										?>
									</ul>
								</li>
								<?php
							}
						}
					}else{
						echo "";
					}
				}
			}else{
				echo "";
			}
			?>
			<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
		</ul>
	</div>
</div>
<div class="sidebar-bg"></div>