<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="coder" />
	<title><?php echo $page.' | '.$this->session->userdata('app_description'); ?></title>
	<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" /> -->
	<link href="<?php echo base_url(); ?>assets/backend/css/default/font-open-sans.css" rel="stylesheet" />
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/app/images/logo/<?php echo $this->session->userdata('app_favicon'); ?>">
	<link href="<?php echo base_url(); ?>assets/backend/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/backend/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/backend/plugins/font-awesome/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/backend/css/default/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/backend/css/default/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/backend/css/default/theme/default.css" rel="stylesheet" id="theme" />
	<link href="<?php echo base_url(); ?>assets/app/plugin/toastr/toastr.min.css" rel="stylesheet" />
	
	<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/app/js/app.js"></script>
</head>
<body>
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
		<?php
		$this->load->view('header');
		$this->load->view('sidebar');
		$this->load->view($content);
		?>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>	
	</div>
	<script src="<?php echo base_url(); ?>assets/backend/js/apps.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/backend/js/theme/default.min.js"></script>
	<!-- Plugins -->
	<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/backend/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/backend/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/app/plugin/bootbox/bootbox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/app/plugin/blockui/jquery.blockui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/app/plugin/toastr/toastr.min.js"></script>
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
</body>
</html>
