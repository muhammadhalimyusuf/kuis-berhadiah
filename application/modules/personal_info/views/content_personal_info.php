<style type="text/css">
	.list-group-hover .list-group-item:hover {
		background-color: #e2e7ec;
	}
</style>
<link href="<?php echo base_url(); ?>assets/app/plugin/lightbox/css/lightbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/app/plugin/lightbox/js/lightbox.min.js"></script>
<script src="<?php echo base_url();?>assets/app/js/personal_info.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			</div>
			<h4 class="panel-title"><?php echo ucwords($page); ?></h4>
		</div>
		<div class="panel-body">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<ul class="list-group list-group-hover">
							<li class="list-group-item">
								<div class="d-flex bd-highlight">
									<div class="p-2 bd-highlight w-25 font-weight-bold">Foto Profil</div>
									<div class="p-2 bd-highlight">
										<a href="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" data-lightbox="gallery-group-1">
											<img height="70px" width="70px" src="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" class="rounded" alt="Foto Profil" />
										</a>
									</div>
								</div>
							</li>
							<?php $hh = $this->session->userdata('id_user') ?>
							<li class="list-group-item">
								<div class="d-flex bd-highlight">
									<div class="p-2 bd-highlight w-25 font-weight-bold">Nama Lengkap</div>
									<div class="p-2 bd-highlight" id="nama_lengkap"></div>
									<div class="ml-auto p-2 bd-highlight text-right"><a href="#" data-toggle="modal" data-target="#exampleModalCenter" onclick="edit_personalnama(<?php $hh; ?>)"> <i class="fa fa-pencil-alt"></i></a></div>
								</div>
							</li>
							<li class="list-group-item">
								<div class="d-flex bd-highlight">
									<div class="p-2 bd-highlight w-25 font-weight-bold">Alamat</div>
									<div class="p-2 bd-highlight" id="alamat"></div>
									<div class="ml-auto p-2 bd-highlight text-right"><a href="#" data-toggle="modal" data-target="#exampleModalCenter1" onclick="edit_personalalamat(<?php $hh; ?>)"> <i class="fa fa-pencil-alt"></i></a></div>
								</div>
							</li>
							<li class="list-group-item">
								<div class="d-flex bd-highlight">
									<div class="p-2 bd-highlight w-25 font-weight-bold">Nama Pengguna</div>
									<div class="p-2 bd-highlight" id="username"></div>
									<div class="ml-auto p-2 bd-highlight text-right"><a href="#" data-toggle="modal" data-target="#exampleModalCenter2" onclick="edit_loginusername(<?php $hh; ?>)"> <i class="fa fa-pencil-alt"></i></a></div>
								</div>
							</li>
							<li class="list-group-item">
								<div class="d-flex bd-highlight">
									<div class="p-2 bd-highlight w-25 font-weight-bold">Kata Sandi</div>
									<div class="p-2 bd-highlight">******</div>
									<div class="ml-auto p-2 bd-highlight text-right"><a href="#" data-toggle="modal" data-target="#exampleModalCenter3" onclick="edit_loginpass()"> <i class="fa fa-pencil-alt"></i></a></div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form action="javascript:void(0)" id="form_nama" class="needs-validation" novalidate>
				<input type="hidden" name="id"/>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalCenterTitle">Ganti Nama Lengkap</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap">
						<div class="valid-feedback">Ok!</div>
						<div class="invalid-feedback"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnSaveNama" onclick="update_personalnama()" class="btn btn-primary btn-sm">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form action="javascript:void(0)" id="form_alamat" class="needs-validation" novalidate>
				<input type="hidden" name="id"/>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalCenterTitle">Ganti Alamat</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat">
						<div class="valid-feedback">Ok!</div>
						<div class="invalid-feedback"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnSaveAlamat" onclick="update_personalalamat()" class="btn btn-primary btn-sm">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form action="javascript:void(0)" id="form_username" class="needs-validation" novalidate>
				<input type="hidden" name="id"/>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalCenterTitle">Ganti Nama Pengguna</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<input type="hidden" name="id"/>
						<input type="text" class="form-control" id="username" name="username" placeholder="Nama Pengguna">
						<div class="valid-feedback">Ok!</div>
						<div class="invalid-feedback"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnSaveUname" onclick="update_loginusername()" class="btn btn-primary btn-sm">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form action="javascript:void(0)" id="form_pass" class="needs-validation" novalidate>
				<input type="hidden" name="id"/>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalCenterTitle">Ganti Kata Sandi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<div class="input-group" id="show_hide_password">
							<input class="form-control" type="password" id="password" name="password" placeholder="Kata Sandi Baru">
							<div class="input-group-addon">
								<a href="#" onclick="pass()"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
							</div>
							<div class="valid-feedback">Ok!</div>
							<div class="invalid-feedback"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnSavePass" onclick="update_loginpass()" class="btn btn-primary btn-sm">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>