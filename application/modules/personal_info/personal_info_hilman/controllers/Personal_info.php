<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Personal_info extends CI_Controller {
	var $tabelna = 'app_login';
	var $idna    = 'user_id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->load->model('Model_personal_info','personal_info');
		$this->db1 = $this->load->database('default', TRUE);
			 	
	}
	public function index(){
		$this->mybreadcrumb->add('Akun', base_url('personal_info'));
		$this->mybreadcrumb->add('Personal Info', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'personal_info';
		$data['page']        = 'Personal Info';
		$data['content']     = 'content_personal_info';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function get_personil_info(){
		if($this->input->is_ajax_request()){
			$id   = $this->session->userdata('user_id');
			$data = $this->personal_info->get_by_id($this->tabelna,$this->idna,$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function process_update(){
		$this->_validate();
		/*if($this->input->is_ajax_request()){
			$this->_validate();
			$id      = $this->input->post($this->idna);
			$user_id = $this->session->userdata('user_id');
			$data = array(
				'personal_info'       => $this->input->post('personal_info'),
				'last_update' => date('Y-m-d H:i:s'),
				'user_id'     => $user_id
			);
			$query = $this->personal_info->update($this->tabelna, $data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}*/
	
	}
	private function _validate(){
		$id   = $this->session->userdata('user_id');
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$this->form_validation->set_rules('username','Username','htmlspecialchars|trim|required|min_length[6]|max_length[100]|is_unique[app_login.username]');
		$this->form_validation->set_rules('password_lama','Password','required|min_length[6]|max_length[100]');
		$this->form_validation->set_rules('password_baru','Password','required|min_length[6]|max_length[100]');
		$this->form_validation->set_rules('password_ulang','Password','required|min_length[6]|max_length[100]|matches[password_baru]');
		if($this->form_validation->run()){
		}else{
			$username = form_error('username');
			if ($username !='') {
				$response['inputerror'][]   = 'username';
				$response['error_string'][] = $username;
				$response['status']         = 'invalid';
			}
			$password_lama = form_error('password_lama');
			if ($password_lama !='') {
				$response['inputerror'][]   = 'password_lama';
				$response['error_string'][] = $password_lama;
				$response['status']         = 'invalid';
			}
			$password_baru = form_error('password_baru');
			if ($password_baru !='') {
				$response['inputerror'][]   = 'password_baru';
				$response['error_string'][] = $password_baru;
				$response['status']         = 'invalid';
			}
			$password_ulang = form_error('password_ulang');
			if ($password_ulang !='') {
				$response['inputerror'][]   = 'password_ulang';
				$response['error_string'][] = $password_ulang;
				$response['status']         = 'invalid';
			}
		}
		if($response['status'] === 'invalid'){
			echo json_encode($response);
			exit();
		}

	}
	
	
}
