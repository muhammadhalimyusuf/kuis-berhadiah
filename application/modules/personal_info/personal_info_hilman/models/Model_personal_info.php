<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_personal_info extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function get_by_id($table,$where,$id){
		$this->db->select('username');
		$this->db->select('email');
		$this->db->select('foto_profil');
		$this->db->from($table);
		$this->db->where($where,$id);
		$query = $this->db->get();
		return $query->row();
	}
	public function update($table, $data, $where){
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}
}