<style type="text/css">
	.list-group-hover .list-group-item:hover {
		background-color: #e2e7ec;
	}
</style>
<link href="<?php echo base_url(); ?>assets/app/plugin/lightbox/css/lightbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/app/plugin/lightbox/js/lightbox.min.js"></script>
<div id="content" class="content">
	<?php echo $breadcrumbs; ?> 
	<h1 class="page-header"><?php echo ucwords($page); ?></h1>
	<div class="panel panel-inverse">
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			</div>
			<h4 class="panel-title"><?php echo ucwords($page); ?></h4>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<ul class="list-group list-group-hover">
						<li class="list-group-item">
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight w-25 font-weight-bold" id="labelFoto">Foto Profil</div>
								<div class="p-2 bd-highlight">
									<a href="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" data-lightbox="gallery-group-1">
										<img height="70px" width="70px" src="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" class="rounded" alt="Foto Profil" />
									</a>
								</div>
								<div class="ml-auto p-2 bd-highlight text-right">
									<div class="ml-auto p-2 bd-highlight text-right"><a href="javascript:void(0)" onclick="modal_edit('Foto')"> <i class="fa fa-pencil-alt"></i></a>
									</div>
								</div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight w-25 font-weight-bold" id="labelEmail">Email</div>
								<div class="p-2 bd-highlight txt_email"></div>
								<div class="ml-auto p-2 bd-highlight text-right"><a href="javascript:void(0)" onclick="modal_edit('Email')"> <i class="fa fa-pencil-alt"></i></a>
								</div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight w-25 font-weight-bold" id="labelUsername">Username</div>
								<div class="p-2 bd-highlight txt_username"></div>
								<div class="ml-auto p-2 bd-highlight text-right"><a href="javascript:void(0)" onclick="modal_edit('Username')"> <i class="fa fa-pencil-alt"></i></a></div>
							</div>
						</li>
						<li class="list-group-item">
							<div class="d-flex bd-highlight">
								<div class="p-2 bd-highlight w-25 font-weight-bold" id="labelPassword">Password</div>
								<div class="p-2 bd-highlight">******</div>
								<div class="ml-auto p-2 bd-highlight text-right"><a href="javascript:void(0)" onclick="modal_edit('Password')"> <i class="fa fa-pencil-alt"></i></a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form action="javascript:void(0)" id="form" class="needs-validation" novalidate>
				<div class="modal-header">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="javascript:void(0)" id="form" class="needs-validation" novalidate>
						<div id="myGroup">
							<div class="form-group collapse" id="collapseFoto" data-parent="#myGroup">
								<center>
									<img height="70px" width="70px" src="<?php echo base_url(); ?>assets/app/images/user/<?php echo $this->session->userdata('pro_foto') ?>" class="rounded m-b-15" alt="Foto Profil" />	
								</center>
								<input type="file" name="foto" id="foto" class="form-control" placeholder="Foto" required/>
							</div>
							<div class="form-group collapse" id="collapseEmail" data-parent="#myGroup">
								<input type="email" name="email" id="email" class="form-control" placeholder="Email" required/>
							</div>
							<div class="form-group collapse" id="collapseUsername" data-parent="#myGroup">
								<input type="text" name="username" id="username" class="form-control" placeholder="Username" required/>
								<div class="valid-feedback"></div>
								<div class="invalid-feedback"></div>
							</div>
							<div class="form-group collapse" id="collapsePassword" data-parent="#myGroup">
								<div class="form-group">
									<input type="text" name="password_lama" id="password_lama" class="form-control" placeholder="Password Lama" required/>
									<div class="valid-feedback"></div>
								<div class="invalid-feedback"></div>
								</div>
								<div class="form-group">
									<input type="text" name="password_baru" id="password_baru" class="form-control" placeholder="Password Baru" required/>
									<div class="valid-feedback"></div>
								<div class="invalid-feedback"></div>
								</div>
								<div class="form-group">
									<input type="text" name="password_ulang" id="password_ulang" class="form-control" placeholder="Ulangi Password Baru" required/>
									<div class="valid-feedback"></div>
								<div class="invalid-feedback"></div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnReset" onclick="default_form()" class="btn btn-default btn-sm">Reset</button>
					<button type="button" id="btnSave" onclick="simpan()" class="btn btn-primary btn-sm">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	var save_method;
	var form;
	var table;
	var listdata = '#list_data';
	var modules  = 'personal_info';
	$(document).ready(function() {
		 save_method = 'add';
		get_data();
	});
	function get_data() {
		default_form();
		load_start();
		$.ajax({
			url : $BASE_URL+modules+"/get_personil_info",
			type: "GET",
			dataType: "JSON",
			success: function(data){
				$(".txt_email").text(data.email);
				$(".txt_username").text(data.username);
				$("#username").val(data.username);
				$("#email").val(data.email);
				load_end();
			},
			error: function (jqXHR, textStatus, errorThrown){
				toastr.error('Fatal Error!', 'ERROR');
				load_end();
			}
		});
	}
	function modal_edit($frm) {
		save_method = 'update';
		$form 		= $frm;
		get_data();
		$("#collapse"+$frm).collapse("show");
		$(".modal-title").text("Edit "+$("#label"+$frm).text());	
		$("#form-modal").modal('show');
	}
	function simpan(){
		$('#btnSave').text('Saving...'); 
		$('#btnSave').attr('disabled',true); 
		var url;
		if(save_method == 'add') {
			url = $BASE_URL+modules+"/process_simpan";
		} else {
			url = $BASE_URL+modules+"/process_update";
		}
		$email          = $("#email").val();
		$username       = $("#username").val();
		$password_lama  = $("#password_lama").val();
		$password_baru  = $("#password_baru").val();
		$password_ulang = $("#password_ulang").val();

		$.ajax({
			url : url,
			type: "POST",
			data: {email:$email, username:$username, password_lama:$password_lama, password_baru:$password_baru, password_ulang:$password_ulang},
			dataType: "JSON",
			success: function(data){
				if(data.status == true)  {
					toastr.success(data.notif, 'SUCCESSFULLY');
					$("#form-modal").modal('hide');
				}else if(data.status == 'invalid'){
					for (var i = 0; i < data.inputerror.length; i++)  {
						$('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
						$('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
						$('[name="'+data.inputerror[i]+'"]').next().next().html(data.error_string[i]);
					}
					$('#btnSave').text('Save'); 
					$('#btnSave').attr('disabled',false);
				}else{
					toastr.error(data.notif, 'FAILED');
					default_form();
				}
			},
			error: function (jqXHR, textStatus, errorThrown){ 
				toastr.error('Fatal Error!', 'ERROR');
				$("#form-modal").modal('hide');
			}
		});
	}
	
</script>