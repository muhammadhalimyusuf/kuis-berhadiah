<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Personal_info extends CI_Controller {
	var $idna    = 'id';
	public function __construct(){
		parent::__construct();
		$this->app->login();
		$this->load->model('Model_personal_info','personal_info');
		$this->db1 = $this->load->database('default', TRUE);
	}
	public function index(){
		$this->mybreadcrumb->add('Akun', base_url('personal_info'));
		$this->mybreadcrumb->add('Personal Info', base_url());
		$data['breadcrumbs'] = $this->mybreadcrumb->render();
		$data['link']        = 'personal_info';
		$data['page']        = 'Personal Info';
		$data['content']     = 'content_personal_info';
		$this->load->view('dashboard/dashboard_view',$data);
	}
	public function get_data(){
		if ($this->input->is_ajax_request()){
			$tabel 	= $this->session->userdata('tipe_user');
			$id 	= $this->session->userdata('user_id');
			$this->db->select($tabel.'.id');
			$this->db->select($tabel.'.nama');
			$this->db->select($tabel.'.alamat');
			$this->db->select('app_login.username');
			$this->db->from('app_login');
			$this->db->join($tabel,'app_login.user_id = '.$tabel.'.id');
			$this->db->where($tabel.'.id',$id);
			$query = $this->db->get();
			$data  = $query->row();
			echo json_encode($data);
		} else {
			redirect('_404','refresh');
		}	
	}
	public function get_personal(){
		if($this->input->is_ajax_request()){
			$tabel 	= $this->session->userdata('tipe_user');
			$id 	= $this->session->userdata('user_id');
			$data 	= $this->personal_info->get_by_id($tabel,$tabel.'.id',$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function update_personal_nama(){
		if ($this->input->is_ajax_request()) {
			$this->_validate();
			$id     	= $this->input->post($this->idna);
			$tabel  	= $this->session->userdata('tipe_user');
			$new_nama	= $this->input->post('nama_lengkap');
			$data 		= array(
				'nama' => $new_nama
			);
			$query  = $this->personal_info->update($tabel,$data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
				$this->session->set_userdata('pro_nama', $new_nama);
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		} else {
			redirect('_404','refresh');
		}
	}
	public function update_personal_alamat(){
		if ($this->input->is_ajax_request()) {
			$this->_validate1();
			$id     	= $this->input->post($this->idna);
			$tabel  	= $this->session->userdata('tipe_user');
			$new_alamat	= $this->input->post('alamat');
			$data 		= array(
				'alamat' => $new_alamat
			);
			$query  = $this->personal_info->update($tabel,$data,array($this->idna => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		} else {
			redirect('_404','refresh');
		}
	}
	public function get_datalogin(){
		if($this->input->is_ajax_request()){
			$tabel 	= 'app_login';
			$id 	= $this->session->userdata('user_id');
			$data 	= $this->personal_info->get_by_id($tabel,$tabel.'.user_id',$id);
			echo json_encode($data);
		}else{
			redirect("_404",'refresh');
		}
	}	
	public function update_login_username(){
		if($this->input->is_ajax_request()){
			$this->_validate2();
			$id 			= $this->session->userdata('user_id');
			$new_username 	= $this->input->post('username');
			$data = array(
				'username' => $new_username
			);
			$query = $this->personal_info->update('app_login', $data,array('user_id' => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	public function update_login_pass(){
		if($this->input->is_ajax_request()){
			$this->_validate3();
			$id 			= $this->session->userdata('user_id');
			$new_password 	= $this->input->post('password');
			$data = array(
				'password' => md5(md5($new_password))
			);
			$query = $this->personal_info->update('app_login', $data,array('user_id' => $id));
			if($query > 0){
				$response['status'] = true;
				$response['notif']  = 'Data berhasil diperbaharui!';
			}else{
				$response['status'] = false;
				$response['notif']  = 'Data gagal diperbaharui!';
			}
			echo json_encode($response);
		}else{
			redirect("_404",'refresh');
		}
	}
	private function _validate(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$nama_lengkap 	= $this->input->post('nama_lengkap');
		if($nama_lengkap == ''){
			$response['inputerror'][]   = 'nama_lengkap';
			$response['error_string'][] = 'nama lengkap harus diisi!';
			$response['status']         = 'invalid';
		}
		if($response['status'] === 'invalid'){
			echo json_encode($response);
			exit();
		}
	}
	private function _validate1(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$alamat 	= $this->input->post('alamat');
		if($alamat == ''){
			$response['inputerror'][]   = 'alamat';
			$response['error_string'][] = 'alamat harus diisi!';
			$response['status']         = 'invalid';
		}
		if($response['status'] === 'invalid'){
			echo json_encode($response);
			exit();
		}
	}
	private function _validate2(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$username 	= $this->input->post('username');
		if($username == ''){
			$response['inputerror'][]   = 'username';
			$response['error_string'][] = 'username harus diisi!';
			$response['status']         = 'invalid';
		}
		if($response['status'] === 'invalid'){
			echo json_encode($response);
			exit();
		}
	}
	private function _validate3(){
		$response                 = array();
		$response['error_string'] = array();
		$response['inputerror']   = array();
		$response['status']       = true;
		$password 	= $this->input->post('password');
		if($password == ''){
			$response['inputerror'][]   = 'password';
			$response['error_string'][] = 'kata sandi baru harus diisi!';
			$response['status']         = 'invalid';
		}
		if($response['status'] === 'invalid'){
			echo json_encode($response);
			exit();
		}
	}
}
