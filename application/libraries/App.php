<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App{
  protected $CI;
  function __construct(){
    $this->CI =& get_instance();
    $this->CI->load->database();
    date_default_timezone_set('Asia/Jakarta');
  }
  public function apps(){   
    $this->CI->db->from('app');
    $this->CI->db->where('id','1');
    $query = $this->CI->db->get();
    return $query;
  }
  function keurlogin(){
    if($this->CI->session->userdata('login') == FALSE){
      return FALSE;
    }
    return TRUE;
  }
  function login(){
    if($this->keurlogin() == FALSE){
      redirect('login','refresh');
    }
  }
  function anti($inp) {
    if(is_array($inp))
      return array_map(__METHOD__, $inp);
    if(!empty($inp) && is_string($inp)) {
      return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
    }
    return $inp;
  } 
  function do_logout(){
    $this->CI->session->sess_destroy();
  }
  function checkfmail($prs){
    $email = $prs; 
    $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
    $email = (preg_match($regex, $email))?$email:"invalid";
    return $email;
  }
  public function role_akses(){
    $uri = $this->CI->uri->segment(1);
    if ($uri == '') {
      $class_menu = 'dashboard';
    }else{
      $class_menu = $uri;
    }
    $this->CI->db->select('id_menu');
    $this->CI->db->from('app_menu');
    $this->CI->db->where('link',$class_menu);
    $id_menu = $this->CI->db->get()->row()->id_menu;
    $level = $this->CI->session->userdata('level');
    $query = $this->CI->db->query("SELECT * FROM  app_akses WHERE app_akses.level = '$level' AND FIND_IN_SET($id_menu,app_akses.menu_id)")->result();
    if(!empty($query)){
      return TRUE;
    }else{
      redirect(site_url('_404'));
    }
  }
  public function cek_mail_exist_add($mail,$tbl)
  {   
    $this->CI->db->from('app_login');
    $this->CI->db->where('app_login.email',$mail);
    $query = $this->CI->db->get();
    if (empty($query->result())) {
      $this->CI->db->from('app_login');
      $this->CI->db->where('app_login.username',$mail);
      $username_query = $this->CI->db->get();  
      if (empty($username_query->result())) {
        $this->CI->db->from($tbl);
        $this->CI->db->where($tbl.'.email',$mail);
        $result_query = $this->CI->db->get();  
        return $result_query;
      }else{
        return $username_query;
      }
    }else{
      return $query;
    }
  }
  public function cek_mail_exist_update($mail,$id,$tbl)
  {   
    $this->CI->db->from($tbl);
    $this->CI->db->where($tbl.'.email',$mail);
    $this->CI->db->where('id !=',$id);
    $query = $this->CI->db->get();
    if (empty($query->result())) {
      $this->CI->db->from('app_login');
      $this->CI->db->where('app_login.email',$mail);
      $this->CI->db->where('app_login.user_id !=',$id);
      $result_query = $this->CI->db->get();  
      return $result_query;
    }else{
      return $query;
    }
  }
  public function cek_uname_exist_update($uname,$id,$tbl)
  {   
    $this->CI->db->from($tbl);
    $this->CI->db->where($tbl.'.username',$uname);
    $this->CI->db->where('id !=',$id);
    $query = $this->CI->db->get();
    if (empty($query->result())) {
      $this->CI->db->from('app_login');
      $this->CI->db->where('app_login.username',$uname);
      $this->CI->db->where('app_login.user_id !=',$id);
      $result_query = $this->CI->db->get();  
      return $result_query;
    }else{
      return $query;
    }
  }
  public function cek_nik_availabe($nik,$tbl){
    $this->CI->db->from($tbl);
    $this->CI->db->where('nik',$nik);
    $query = $this->CI->db->get();
      // if (empty($query->result())) {
      //   $this->CI->db->from($tbl);
      //   $this->CI->db->where('nik',$nik);
      //   $result_query = $this->CI->db->get();  
      //   return $result_query;
      // }else{
    return $query;
      // }
  }
  public function cek_username_availabel($username){
    $this->CI->db->from('app.username');
    $this->CI->db->where('username',$username);
    $query = $this->CI->db->get();
    return $query;
  } 
  public function check_valid_number($number){
    if (is_numeric($number)) { $result = 'true'; return $result; } else { $result =  'false'; return $result; }
  } 
  public function user_save($data2){
    $this->CI->db->insert('app_login', $data2);
    return $this->CI->db->insert_id();  
  }
  public function update_user($data2, $where){
    $this->CI->db->update('app_login', $data2, $where);
    return $this->CI->db->affected_rows();
  }
  public function user_delete_by_id($id){
    $this->CI->db->where('user_id', $id);
    $this->CI->db->delete('app_login');
  }
  public function cek_availabe($tbl,$field,$value){
    $this->CI->db->from($tbl);
    $this->CI->db->where($field,$value);
    $query = $this->CI->db->get();
    return $query;
  }
  public function cek_availabe_update($tbl,$field,$value,$id){
   $this->CI->db->from($tbl);
   $this->CI->db->where($tbl.'.'.$field,$value);
   $this->CI->db->where('id !=',$id);
   $query = $this->CI->db->get();
   if (empty($query->result())) {
    $this->CI->db->from($tbl);
    $this->CI->db->where($tbl.'.'.$field,$value);
    $this->CI->db->where($tbl.'.id !=',$id);
    $result_query = $this->CI->db->get();  
    return $result_query;
  }else{
    return $query;
  }
}
function getMonthList() {
  $months = array('' => 'Pilih Bulan' ,1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December');
  return $months;
}
function penyebut($nilai) {
  $nilai = abs($nilai);
  $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  $temp = "";
  if ($nilai < 12) {
    $temp = " ". $huruf[$nilai];
  } else if ($nilai <20) {
    $temp = $this->penyebut($nilai - 10). " belas";
  } else if ($nilai < 100) {
    $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
  } else if ($nilai < 200) {
    $temp = " seratus" . $this->penyebut($nilai - 100);
  } else if ($nilai < 1000) {
    $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
  } else if ($nilai < 2000) {
    $temp = " seribu" . penyebut($nilai - 1000);
  } else if ($nilai < 1000000) {
    $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
  } else if ($nilai < 1000000000) {
    $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
  } else if ($nilai < 1000000000000) {
    $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
  } else if ($nilai < 1000000000000000) {
    $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
  }     
  return $temp;
}
function terbilang($nilai) {
  if($nilai<0) {
    $hasil = "minus ". trim($this->penyebut($nilai));
  } else {
    $hasil = trim($this->penyebut($nilai));
  }         
  return $hasil;
}
}