
var modules  = 'report_kunjungan_hotel';

function removeOptions(selectbox){
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}

$(document).ready(function() {
    $('#report_kh_tahun').select2({
        width: "125px",
        placeholder: "Pilih Tahun",
        data: [
                { "id": "2020","text": "2020" },
                { "id": "2021","text": "2021" },
            ]
    });
    $('#report_kh_bulan').select2({
        width: "125px",
        placeholder: "Pilih Bulan",
        data: [
                { "id": "","text": "" },
                { "id": "01","text": "Januari" },
                { "id": "02","text": "Februari" },
                { "id": "03","text": "Maret" },
                { "id": "04","text": "April" },
                { "id": "05","text": "Mei" },
                { "id": "06","text": "Juni" },
                { "id": "07","text": "Juli" },
                { "id": "08","text": "Agustus" },
                { "id": "09","text": "September" },
                { "id": "10","text": "Oktober" },
                { "id": "11","text": "November" },
                { "id": "12","text": "Desember" }
            ]
    });
    $.ajax({
        url: $BASE_URL+modules+"/getDataKategori",
        dataType: 'json',
        success: function (datas) {
            var kategori = $.map(datas, function (obj) {
                obj.id = obj.id || obj.id;
                obj.text = obj.text || obj.kategori;
                return obj;
            }); 
            $('#report_kh_kategori').select2({
                width: "170px",
                placeholder: "Pilih Kategori Hotel",
                data: kategori
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("error");
        }
    });
    $.ajax({
        url: $BASE_URL+modules+"/getDataHotel",
        dataType: 'json',
        success: function (datas) {
            var hotel = $.map(datas, function (obj) {
                obj.id = obj.id || obj.id;
                obj.text = obj.text || obj.nama;
                return obj;
            }); 
            $('#report_kh_hotel').select2({
                width: "170px",
                placeholder: "Pilih Nama Hotel",
                data: hotel
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("error");
        }
    });
    $('#report_kh_kategori').change( function () {
        var id_kategori = $(this).val();
        var hotel = [];
        removeOptions(document.getElementById("report_kh_hotel"));
        $.ajax({
            url: $BASE_URL+modules+"/getDataHotelByKat",
            dataType: "json",
            type: "POST",
            data: {id_kat:id_kategori},
            success: function (datas) {
                var hotel = $.map(datas, function (obj) {
                    obj.id = obj.id || obj.id;
                    obj.text = obj.text || obj.nama;
                    return obj;
                }); 
                $('#report_kh_hotel').select2({
                    width: "170px",
                    placeholder: "Pilih Nama Hotel",
                    data: hotel
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("error");
            }
        });
    });
    
});
function tombol_laporan()
{
    $('#form_laporan')[0].reset();
    $('#date').val(null);
    $('#report_kh_tahun').val("2020").trigger('change');
    $('#report_kh_bulan').val(null).trigger('change');
    $('#report_kh_kategori').val(null).trigger('change');
    $('#report_kh_hotel').val(null).trigger('change');
}

