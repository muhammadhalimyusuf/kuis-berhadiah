var save_method;
var table;
var listdata = '#list_data';
var modules  = 'input_kunjungan_hotel';
$(document).ready(function() {
    save_method = 'add';
    table = $('#list_data').DataTable({ 
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+modules+"/get_data",
            "type": "POST"
        },
        "columnDefs": [
        { "width": "3%", "targets": 0 },
        { "width": "30%", "targets": 1 },
        { "width": "30%", "targets": 2 },
        { "width": "20%", "targets": 3 },
        { "width": "17%", "targets": 4 }
        ],
        "aoColumns": [
        { "bSortable": false },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": false }
        ],
    });
    document.getElementById('formManca').style.display = 'none';
});

function myFunction() {
    $('s_wisman').val(null).trigger('change');
    $(".barisNegara").remove();
    // Get the checkbox
    var checkBox = document.getElementById("cekManca");
    // Get the output text
    var text = document.getElementById("formManca");
    // If the checkbox is checked, display the output text
    if (checkBox.checked == true){
        text.style.display = "block";
    } else {
        text.style.display = "none";
    }
}
function detail_data(id) {
    $('#modaldetilKunjunganTitle').html('Detail Data Kunjungan Hotel'); // Set Title to Bootstrap modal title
    $.ajax({
        url: $BASE_URL+modules+"/get_data_hotel_kunjungan",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function (data) {
            var tgll = data['data_kunjungan'].create_at;
            var explode = tgll.split("-");
            var bulan = explode[1]; 
            switch (bulan) {
                case"01": bulan="Januari"; break;
                case"02": bulan="Februari"; break;
                case"03": bulan="Maret"; break;
                case"04": bulan="April"; break;
                case"05": bulan="Mei"; break;
                case"06": bulan="Juni"; break;
                case"07": bulan="Juli"; break;
                case"08": bulan="Agustus"; break;
                case"09": bulan="September"; break;
                case"10": bulan="Oktober"; break;
                case"11": bulan="November"; break;
                case"12": bulan="Desember"; break;
            }
            $('#bulan_lapor').html(bulan+" "+explode[0]);
            $('#nama_hotel').html(data['data_kunjungan'].nama_hotel);
            $('#alamat_hotel').html(data['data_kunjungan'].alamat_hotel);
            $('#rata_tinggal').html(data['data_kunjungan'].ratarata_menginap+" Hari");
            $('#sum_wisnus').html(data['data_kunjungan'].sum_wisnus+" Orang");
            var jm_wisman;
            if (data['data_kunjungan'].total_wisman != null) {
                jm_wisman = data['data_kunjungan'].total_wisman+" Orang";
            } else {
                jm_wisman = 0+" Orang";
            }
            $('#tot_wisman').html(jm_wisman);
            if (data['detil_wisman'].length > 0) {
                $('#tabel_wisman').DataTable({
                    "searching": false,
                    "destroy": true,
                    "data": data['detil_wisman'],
                    "deferRender": true,
                    "columns": [
                    { "data": "datana.no"},
                    { "data": "datana.nama_negara"},
                    { "data": "datana.jumlah_wisman"},
                    ]
                });
                $('#tabel_wisman').dataTable().fnClearTable();
                $('#tabel_wisman').dataTable().fnAddData(data['detil_wisman']);
            }else{
                $('#tabel_wisman').DataTable({
                    "searching": false,
                    "destroy": true,
                    "deferRender": true,
                    "sEcho": 0,
                    "iTotalRecords": "0",
                    "iTotalDisplayRecords": "0",
                    "aaData": [
                    ]
                });
            }
        }
    });
}
function tambah_data() {
    var text = document.getElementById("formManca");
    text.style.display = "none";
    save_method = 'add';
    $('#btnSave').text('Simpan'); 
    $('#btnSave').attr('disabled',false);
    $('#form')[0].reset(); // reset form on modals
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $('#modalKunjunganTitle').text('Tambah Data Kunjungan Hotel'); // Set Title to Bootstrap modal title
    $.unblockUI();
    
}
function simpan(){ 
    $('#btnSave').text('Menyimpan...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+modules+"/process_simpan";
    } else {
        url = $BASE_URL+modules+"/process_update";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                Swal.fire(
                    'Good job!',
                    'Terimakasih atas partisipasi Saudara telah berkontribusi dalam pembangunan Pariwisata Kota Tasikmalaya...',
                    'success'
                );
                toastr.success(data.notif, 'SUCCESSFULLY');
                setTimeout(function(){
                   $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                page_reset();
            }else if(data.status == 'invalid'){
                for (var i = 0; i <= data.inputerror.length; i++)  {
                    if (data.error_string[i] != '') {
                        $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                        $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                        $('[name="'+data.inputerror[i]+'"]').next().next().html(data.error_string[i]);
                    }
                }
                $('#btnSave').text('Simpan'); 
                $('#btnSave').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
                setTimeout(function(){
                   $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                page_reset();
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
            page_reset();
        }
    });
}
function edit_data(id){
    $('#btnReset').hide();
    page_reset();
    save_method = 'update';
    $('#modalKunjunganTitle').text('Edit Data Kunjungan Hotel'); // Set Title to Bootstrap modal title
    $.ajax({
        url : $BASE_URL+modules+"/get_data_by_id",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function(data){
            $('[name="id"]').val(data.id);
            $('[name="pilih_hotel"]').val(data.nama);
            $('[name="hotel_id"]').val(data.hotel_id);
            $('[name="wisnus"]').val(data.sum_wisnus);
        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
            page_reset();
        }
    });
}
function update(){
    $('#btnUpdate').text('Menyimpan...'); 
    $('#btnUpdate').attr('disabled',true); 
    var url = $BASE_URL+modules+"/process_update";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formUpdate').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                toastr.success(data.notif, 'SUCCESSFULLY');
                setTimeout(function(){
                   $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                update_reset();
            }else if(data.status == 'invalid'){
                for (var i = 0; i <= data.inputerror.length; i++)  {
                    if (data.error_string[i] != '') {
                        $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                        $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                        $('[name="'+data.inputerror[i]+'"]').next().next().html(data.error_string[i]);
                    }
                }
                $('#btnUpdate').text('Simpan'); 
                $('#btnUpdate').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
                setTimeout(function(){
                    $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                update_reset();
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
            setTimeout(function(){
                $("[data-dismiss=modal]").trigger({ type: "click" });
            },100);
            reload_table(listdata);
            update_reset();
        }
    });
}
function hapus_data (id, pesan){
    page_reset();
    bootbox.confirm({
        message: "Yakin akan menghapus <b>" +pesan+ "</b> berikut ?", 
        callback: function(result) {
            if (result) {
                $.ajax({
                    url :  $BASE_URL+modules+"/process_hapus",
                    type: "POST",
                    dataType: "JSON",
                    data: {id:id},
                    success: function(data){
                        if (data.status == true) {
                            toastr.success(data.notif, 'SUCCESSFULLY');
                        }else{
                            toastr.error(data.notif, 'FAILED');
                        }
                        reload_table(listdata);
                        $.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        toastr.error('Fatal Error!', 'ERROR');
                    }
                });
            }
        }
    });
}
function page_reset() {
    document.getElementById('formManca').style.display = 'none';
    $(document).on('click', '.remove', function(){
        $(this).closest('tr').remove();
    });
    $('#btnSave').text('Simpan'); 
    $('#btnSave').attr('disabled',false);
    $('#form')[0].reset();
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $.unblockUI();
}
