var save_method;
var table;
var listdata = '#list_data';
var modules  = 'personal_info';
$(document).ready(function(id) {
    save_method = 'add';
    $.ajax({
        url      : $BASE_URL+modules+"/get_data",
        type     : "POST",
        dataType : "JSON",
        data     : {id:id},
        success: function(data){
            $('#nama_lengkap').text(data.nama);
            $('#alamat').text(data.alamat);
            $('#username').text(data.username);
        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
        }
    })
});
function edit_personalnama(id){
    $('#btnSaveNama').text('Update');
    $('#btnSaveNama').attr('disabled',false);
    $('#form_nama')[0].reset(); 
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $.unblockUI();
    $.ajax({
        url : $BASE_URL+modules+"/get_personal",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function(data){
            $('[name="id"]').val(data.id);
            $('[name="nama_lengkap"]').val(data.nama);
        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function update_personalnama(){
    $('#btnSaveNama').text('Saving...'); 
    $('#btnSaveNama').attr('disabled',true); 
    var url = $BASE_URL+modules+"/update_personal_nama";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_nama').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                setTimeout(function () {
                    location.reload();
                },1000);
                toastr.success(data.notif, 'SUCCESSFULLY');
            }else if(data.status == 'invalid'){
                for (var i = 0; i < data.inputerror.length; i++)  {
                    $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                    $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                }
                $('#btnSaveNama').text('Update'); 
                $('#btnSaveNama').attr('disabled',false);
            }else{
                setTimeout(function () {
                    location.reload();
                },1000);
                toastr.error(data.notif, 'FAILED');
            }
        },
        error: function (jqXHR, textStatus, errorThrown){
            setTimeout(function () {
                location.reload();
            },1000); 
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function edit_personalalamat(id){
    $('#btnSaveAlamat').text('Update');
    $('#btnSaveAlamat').attr('disabled',false);
    $('#form_alamat')[0].reset(); 
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $.unblockUI();
    $.ajax({
        url : $BASE_URL+modules+"/get_personal",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function(data){
            $('[name="id"]').val(data.id);
            $('[name="alamat"]').val(data.alamat);
        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function update_personalalamat(){
    $('#btnSaveAlamat').text('Saving...'); 
    $('#btnSaveAlamat').attr('disabled',true); 
    var url = $BASE_URL+modules+"/update_personal_alamat";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_alamat').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                setTimeout(function () {
                    location.reload();
                },1000);
                toastr.success(data.notif, 'SUCCESSFULLY');
            }else if(data.status == 'invalid'){
                for (var i = 0; i < data.inputerror.length; i++)  {
                    $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                    $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                }
                $('#btnSaveAlamat').text('Save'); 
                $('#btnSaveAlamat').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function edit_loginusername(id){ 
    $('#btnSaveUname').text('Update');
    $('#btnSaveUname').attr('disabled',false);
    $('#form_username')[0].reset(); 
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $.unblockUI();
    $.ajax({
        url : $BASE_URL+modules+"/get_datalogin",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function(data){
            $('[name="id"]').val(data.id);
            $('[name="username"]').val(data.username);
        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function update_loginusername() {
    $('#btnSaveUname').text('Saving...'); 
    $('#btnSaveUname').attr('disabled',true); 
    var url = $BASE_URL+modules+"/update_login_username";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_username').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                setTimeout(function () {
                    location.reload();
                },1000);
                toastr.success(data.notif, 'SUCCESSFULLY');
            }else if(data.status == 'invalid'){
                for (var i = 0; i < data.inputerror.length; i++)  {
                    $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                    $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                }
                $('#btnSaveUname').text('Save'); 
                $('#btnSaveUname').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function edit_loginpass(){
    $('#btnSavePass').text('Update'); 
    $('#btnSavePass').attr('disabled',false);
    $('#form_pass')[0].reset(); 
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $.unblockUI();
}
function update_loginpass() {
    $('#btnSavePass').text('Saving...'); 
    $('#btnSavePass').attr('disabled',true); 
    var url = $BASE_URL+modules+"/update_login_pass";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_pass').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                setTimeout(function () {
                    location.reload();
                },1000);
                toastr.success(data.notif, 'SUCCESSFULLY');
            }else if(data.status == 'invalid'){
                for (var i = 0; i < data.inputerror.length; i++)  {
                    $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                    $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                }
                $('#btnSavePass').text('Save'); 
                $('#btnSavePass').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
        }
    });
}
function page_reset() {
    save_method = 'add';
    $('#btnSave').text('Save'); 
    $('#btnSave').attr('disabled',false);
    $('#form')[0].reset(); 
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $('.form-title').text('Form Input Data Agama');
    $.unblockUI();
}
function pass() {
    if($('#show_hide_password input').attr("type") == "text"){
        $('#show_hide_password input').attr('type', 'password');
        $('#show_hide_password i').addClass( "fa-eye-slash" );
        $('#show_hide_password i').removeClass( "fa-eye" );
    }else if($('#show_hide_password input').attr("type") == "password"){
        $('#show_hide_password input').attr('type', 'text');
        $('#show_hide_password i').removeClass( "fa-eye-slash" );
        $('#show_hide_password i').addClass( "fa-eye" );
    }
}