var modules  = 'report_data_hotel';
$(document).ready(function() {
    $('#report_dh_status').select2({
        width: "170px",
        placeholder: "Pilih status Hotel",
        data: [
                { "id": "1","text": "Aktif" },
                { "id": "0","text": "Non Aktif" }
        ]
    });
    $.ajax({
        url: $BASE_URL+modules+"/getDataKategori",
        dataType: 'json',
        success: function (datas) {
            var kategori = $.map(datas, function (obj) {
                obj.id = obj.id || obj.id;
                obj.text = obj.text || obj.kategori;
                return obj;
            }); 
            $('#report_dh_kategori').select2({
                width: "170px",
                placeholder: "Pilih Kategori Hotel",
                data: kategori
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("error");
        }
    });
});
function tombol_laporan()
{   
    $('#form_laporan')[0].reset();
    $('#date').val(null);
    $('#report_dh_status').val(1).trigger('change');
    $('#report_dh_kategori').val(null).trigger('change');
}

