var host = window.location.host;
$BASE_URL = 'http://' + host + '/';
$(document).ready(function() {
	$('.sub-menu .active').parent().closest('.has-sub').addClass('active');
	$('.nav-profile .active').parent().closest('.nav-profile').addClass('active');
	$('#form input').on('keyup', function() {
		$(this).removeClass('is-invalid').addClass('is-valid');
	});
	// $('#form select').on('change', function () {
	// 	$(this).removeClass('is-invalid').addClass('is-valid');
	// });
	$('#formUpdate input').on('keyup', function() {
		$(this).removeClass('is-invalid').addClass('is-valid');
	});
	$('#formUpdate select').on('change', function () {
		$(this).removeClass('is-invalid').addClass('is-valid');
	});
});
function logout(nama){
	bootbox.confirm({
		message : nama+" apakah yakin akan keluar ?",
		callback: function(result) {
			if (result) {
				jQuery.blockUI({ 
					message: 'Harap Tunggu ...',
					css: {
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					}
				});
				setTimeout(function(){
					$.unblockUI();
				},500);
				$.ajax({
					url : $BASE_URL+'login/logout',
					complete : function(response) {
						$.unblockUI();
						window.location.href = $BASE_URL;
					}
				});             
			}
		}
	});
}
function reload_table(tblid){
	var tableid = $(tblid).DataTable();
	tableid.ajax.reload(null,false);
}
