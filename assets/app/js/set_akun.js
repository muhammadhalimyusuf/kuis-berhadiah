var save_method;
var table;
var listdata = '#list_data';
var modules  = 'set_akun';
$(document).ready(function() {
    save_method = 'add';
    table = $('#list_data').DataTable({ 
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+modules+"/get_data",
            "type": "POST"
        },
        "columnDefs": [
        { "width": "5%", "targets": 0 },
        { "width": "15%", "targets": 1 },
        { "width": "30%", "targets": 2 },
        { "width": "35%", "targets": 3 },
        { "width": "15%", "targets": 4 },

        ],
        "aoColumns": [
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": false }
        ],
    });
    $('input').change(function () {
        if ($('input[value="1"]').is(':checked')) {
            $('#menuu').show();
        } else {
            $('#menuu').hide();
        }
    }).change();

    
    
});
function tambah_data() {
    save_method = 'add';
    $('#btnSave').text('Simpan'); 
    $('#btnSave').attr('disabled',false);
    $('#form')[0].reset(); // reset form on modals
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $('#modalHotelTitle').text('Tambah Menu Aplikasi'); // Set Title to Bootstrap modal title
    $('#gmenu_utama').hide();
    $('#gnama_akun').hide();
    $.unblockUI();
    $.ajax({
        url: $BASE_URL+modules+"/kel_akun",
        type: "POST",
        dataType: "JSON",
        success: function (data) {

            var $mySelect = $('#kel_akun');

            var awal = '<option value="" disabled selected>Pilih Kelompok Menu</option>';
            var baru = '<option value="-1" >Buat Kelompok Menu Baru</option>';
            var batas= '<option value="" disabled>------------------------</option>'
            var html = '';
            for (var i=0; i<data.length; i++) {
                html += '<option value='+data[i].id_akun+'>'+data[i].nama_akun+'</option>';
            }
            $('#kel_akun').html(awal+baru+batas+html);
        }   
    });
    $('#kel_akun').change(function () {
        var idkel_akun = $(this).val();
        $.ajax({
            url: $BASE_URL+modules+"/menu_utama",
            type: "POST",
            dataType: "JSON",
            data: {parent_id: idkel_akun},
            success: function (data1) {
                console.log(data1);
                console.log(idkel_akun);
                if (idkel_akun > 0) {
                    $('#gnama_akun').hide('fast');
                    $('#gmenu_utama').show('fast');
                    var awal = '<option value="" disabled selected>Pilih Menu Utama</option>';
                    var baru = '<option value="-1" >Buat Menu Utama Baru</option>';
                    var batas= '<option value="" disabled>------------------------</option>'
                    var html = '';
                    var i;
                    for (i=0; i<data1.length; i++) {
                        html += '<option value='+data1[i].id_akun+'>'+data1[i].nama_akun+'</option>';
                    }
                    $('#menu_utama').html(awal+baru+batas+html);
                } else {
                    $('#gmenu_utama').hide('fast');
                    $('#menu_utama').val(null);
                    $('#gnama_akun').show('fast');

                }
            }   
        });
    });
    $('#menu_utama').change(function () {
        var idmenu_utama = $(this).val();
        $.ajax({
            url: $BASE_URL+modules+"/menu_utama",
            type: "POST",
            dataType: "JSON",
            data: {parent_id: idkel_akun},
            success: function (data1) {
                console.log(data1);
                console.log(idkel_akun);
                if (idkel_akun > 0) {
                    $('#gnama_akun').hide('fast');
                    $('#gmenu_utama').show('fast');
                    var awal = '<option value="" disabled selected>Pilih Menu Utama</option>';
                    var baru = '<option value="-1" >Buat Menu Utama Baru</option>';
                    var batas= '<option value="" disabled>------------------------</option>'
                    var html = '';
                    var i;
                    for (i=0; i<data1.length; i++) {
                        html += '<option value='+data1[i].id_akun+'>'+data1[i].nama_akun+'</option>';
                    }
                    $('#menu_utama').html(awal+baru+batas+html);
                } else {
                    $('#gmenu_utama').hide('fast');
                    $('#menu_utama').val(null);
                    $('#gnama_akun').show('fast');

                }
            }   
        });
    });
}
function simpan(){ 
    $('#btnSave').text('Menyimpan...'); 
    $('#btnSave').attr('disabled',true);
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+modules+"/process_simpan";
    } else {
        url = $BASE_URL+modules+"/process_update";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                console.log(data);
                toastr.success(data.notif, 'SUCCESSFULLY');
                setTimeout(function(){
                   $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                page_reset();
            }else if(data.status == 'invalid'){
                for (var i = 0; i <= data.inputerror.length; i++)  {
                    if (data.error_string[i] != '') {
                        $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                        $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                        $('[name="'+data.inputerror[i]+'"]').next().next().html(data.error_string[i]);
                    }
                }
                $('#btnSave').text('Simpan'); 
                $('#btnSave').attr('disabled',false);
            }else{
                console.log(data);
                toastr.error(data.notif, 'FAILED');
                setTimeout(function(){
                   $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                page_reset();
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            console.log(jqXHR);
            toastr.error('Fatal Error!', 'ERROR');
            page_reset();
        }
    });
}
function edit_data(id){
    $('#btnReset').hide();
    page_reset();
    save_method = 'update';
    $.ajax({
        url : $BASE_URL+modules+"/get_data_by_id",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function(data){
            console.log(data);
            $('#modalHotelTitle').text('Form Edit Data Hotel');
            $('[name="id"]').val(data.id);
            $('[name="nama_hotel"]').val(data.nama);
            $('[name="alamat_hotel"]').val(data.alamat);
            $.ajax({
                url: $BASE_URL+modules+"/get_kategori",
                type: "POST",
                dataType: "JSON",
                success: function (response) {
                    var pertama = '<option value="" disabled selected>Pilih Kategori Hotel</option>';
                    var html = '';
                    var i;
                    var sel = data.kategori_id;
                    for (i=0; i<response.length; i++) {
                        var jwb = (response[i].id == sel) ? "selected": null;
                        html += '<option value='+response[i].id+' '+jwb+'>'+response[i].kategori+'</option>';    
                    }
                    
                    $('#kategori_hotel').html(pertama+html);
                }   
            });

        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
            page_reset();
        }
    });
}
function update(){
    $('#btnUpdate').text('Menyimpan...'); 
    $('#btnUpdate').attr('disabled',true); 
    var url = $BASE_URL+modules+"/process_update";
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formUpdate').serialize(),
        dataType: "JSON",
        success: function(data){
            console.log(data);
            if(data.status == true)  {
                toastr.success(data.notif, 'SUCCESSFULLY');
                setTimeout(function(){
                   $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                update_reset();
            }else if(data.status == 'invalid'){
                for (var i = 0; i <= data.inputerror.length; i++)  {
                    if (data.error_string[i] != '') {
                        $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                        $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                        $('[name="'+data.inputerror[i]+'"]').next().next().html(data.error_string[i]);
                    }
                }
                $('#btnUpdate').text('Simpan'); 
                $('#btnUpdate').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
                setTimeout(function(){
                    $("[data-dismiss=modal]").trigger({ type: "click" });
                },100);
                reload_table(listdata);
                update_reset();
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
            setTimeout(function(){
                $("[data-dismiss=modal]").trigger({ type: "click" });
            },100);
            reload_table(listdata);
            update_reset();
        }
    });
}
function hapus_data (id, pesan){
    page_reset();
    bootbox.confirm({
        message: "Yakin akan menghapus <b>" +pesan+ "</b> berikut ?", 
        callback: function(result) {
            if (result) {
                $.ajax({
                    url :  $BASE_URL+modules+"/process_hapus",
                    type: "POST",
                    dataType: "JSON",
                    data: {id:id},
                    success: function(data){
                        if (data.status == true) {
                            toastr.success(data.notif, 'SUCCESSFULLY');
                        }else{
                            toastr.error(data.notif, 'FAILED');
                        }
                        reload_table(listdata);
                        $.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        toastr.error('Fatal Error!', 'ERROR');
                    }
                });
            }
        }
    });
}
function status_aktif(id,pesan){
    page_reset();
    bootbox.confirm({
        message: "Aktifkan <b>" +pesan+ "</b> Berikut ?", 
        callback: function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        message: 'Harap Tunggu ...',
                        css: {
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        }
                    });
                    $.ajax({
                        url :  $BASE_URL+modules+"/status_aktif",
                        type: "POST",
                        dataType: "JSON",
                        data: {id:id},
                        success: function(data){
                            if (data.status == true) {
                                toastr.success(data.notif, 'SUCCESSFULLY');
                            }else{
                                toastr.error(data.notif, 'FAILED');
                            }
                            reload_table(listdata);
                            $.unblockUI();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            toastr.error('Fatal Error!', 'ERROR');
                        }
                    });
                }, 500);
            }
        }
    });
}
function status_nonaktif(id,pesan){
    page_reset();
    bootbox.confirm({
        message: "Non Aktifkan <b>" +pesan+ "</b> Berikut ?", 
        callback: function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        message: 'Harap Tunggu ...',
                        css: {
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        }
                    });
                    $.ajax({
                        url :  $BASE_URL+modules+"/status_nonaktif",
                        type: "POST",
                        dataType: "JSON",
                        data: {id:id},
                        success: function(data){
                            if (data.status == true) {
                                toastr.success(data.notif, 'SUCCESSFULLY');
                            }else{
                                toastr.error(data.notif, 'FAILED');
                            }
                            reload_table(listdata);
                            $.unblockUI();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            toastr.error('Fatal Error!', 'ERROR');
                        }
                    });
                }, 500);
            }
        }
    });
}
function page_reset() {
    $('#btnSave').text('Simpan'); 
    $('#btnSave').attr('disabled',false);
    $('#form')[0].reset();
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $.unblockUI();
}