var save_method;
var table;
var listdata = '#list_data';
var modules  = 'rd_jabatan';
$(document).ready(function() {
    save_method = 'add';
    table = $('#list_data').DataTable({ 
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "order": [], 
        "ajax": {
            "url": $BASE_URL+modules+"/get_data",
            "type": "POST"
        },
        "columnDefs": [
        { "width": "3%", "targets": 0 },
        { "width": "77%", "targets": 1 },
        { "width": "5%", "targets": 2 },
        { "width": "15%", "targets": 3 }
        ],
        "aoColumns": [
        { "bSortable": false },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": false }
        ],
    });
});
function simpan(){
    $('#btnSave').text('Saving...'); 
    $('#btnSave').attr('disabled',true); 
    var url;
    if(save_method == 'add') {
        url = $BASE_URL+modules+"/process_simpan";
    } else {
        url = $BASE_URL+modules+"/process_update";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status == true)  {
                toastr.success(data.notif, 'SUCCESSFULLY');
                reload_table(listdata);
                page_reset();
            }else if(data.status == 'invalid'){
                for (var i = 0; i < data.inputerror.length; i++)  {
                    $('[name="'+data.inputerror[i]+'"]').removeClass('is-valid'); 
                    $('[name="'+data.inputerror[i]+'"]').addClass('is-invalid'); 
                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                }
                $('#btnSave').text('Save'); 
                $('#btnSave').attr('disabled',false);
            }else{
                toastr.error(data.notif, 'FAILED');
                page_reset();
            }
        },
        error: function (jqXHR, textStatus, errorThrown){ 
            toastr.error('Fatal Error!', 'ERROR');
            page_reset();
        }
    });
}
function edit_data(id){
    page_reset();
    save_method = 'update';
    $.ajax({
        url : $BASE_URL+modules+"/get_data_by_id",
        type: "POST",
        dataType: "JSON",
        data: {id:id},
        success: function(data){
            $('.form-title').text('Form Edit Data Jabatan');
            $('[name="id"]').val(data.id);
            $('[name="jabatan"]').val(data.jabatan);
        },
        error: function (jqXHR, textStatus, errorThrown){
            toastr.error('Fatal Error!', 'ERROR');
            page_reset();
        }
    });
}
function hapus_data (id, pesan){
    page_reset();
    bootbox.confirm({
        message: "Yakin akan menghapus <b>" +pesan+ "</b> berikut ?", 
        callback: function(result) {
            if (result) {
                $.ajax({
                    url :  $BASE_URL+modules+"/process_hapus",
                    type: "POST",
                    dataType: "JSON",
                    data: {id:id},
                    success: function(data){
                        if (data.status == true) {
                            toastr.success(data.notif, 'SUCCESSFULLY');
                        }else{
                            toastr.error(data.notif, 'FAILED');
                        }
                        reload_table(listdata);
                        $.unblockUI();
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        toastr.error('Fatal Error!', 'ERROR');
                    }
                });
            }
        }
    });
}
function status_aktif(id,pesan){
    page_reset();
    bootbox.confirm({
        message: "Aktifkan <b>" +pesan+ "</b> Berikut ?", 
        callback: function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        message: 'Harap Tunggu ...',
                        css: {
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        }
                    });
                    $.ajax({
                        url :  $BASE_URL+modules+"/status_aktif",
                        type: "POST",
                        dataType: "JSON",
                        data: {id:id},
                        success: function(data){
                            if (data.status == true) {
                                toastr.success(data.notif, 'SUCCESSFULLY');
                            }else{
                                toastr.error(data.notif, 'FAILED');
                            }
                            reload_table(listdata);
                            $.unblockUI();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            toastr.error('Fatal Error!', 'ERROR');
                        }
                    });
                }, 500);
            }
        }
    });
}
function status_nonaktif(id,pesan){
    page_reset();
    bootbox.confirm({
        message: "Non Aktifkan <b>" +pesan+ "</b> Berikut ?", 
        callback: function(result) {
            if (result) {
                setTimeout(function () {
                    jQuery.blockUI({
                        message: 'Harap Tunggu ...',
                        css: {
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        }
                    });
                    $.ajax({
                        url :  $BASE_URL+modules+"/status_nonaktif",
                        type: "POST",
                        dataType: "JSON",
                        data: {id:id},
                        success: function(data){
                            if (data.status == true) {
                                toastr.success(data.notif, 'SUCCESSFULLY');
                            }else{
                                toastr.error(data.notif, 'FAILED');
                            }
                            reload_table(listdata);
                            $.unblockUI();
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            toastr.error('Fatal Error!', 'ERROR');
                        }
                    });
                }, 500);
            }
        }
    });
}
function page_reset() {
    save_method = 'add';
    $('#btnSave').text('Save'); 
    $('#btnSave').attr('disabled',false);
    $('#form')[0].reset(); 
    $('.form-control').removeClass('is-invalid');
    $('.form-control').removeClass('is-valid');
    $('.form-title').text('Form Input Data Jabatan');
    $.unblockUI();
}