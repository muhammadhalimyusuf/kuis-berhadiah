var host  = window.location.host;
$BASE_URL = 'http://' + host + '/';
function doLogin() {
    $('.needs-validation').addClass('was-validated'); 
    var uname    = jQuery("#username").val();
    var password = jQuery("#password").val();
    if (uname != '' && password != '') {
        jQuery.blockUI({ 
            message: 'Harap Tunggu ...',
            css: {
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            }
        });
        jQuery.ajax({
            url: $BASE_URL + 'login/do_login',
            data: {
                txtuser: uname,
                txtpass: password
            },
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                jQuery.unblockUI();
                if (data.response == 'true') {
                    jQuery.blockUI({
                        css: {
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        }
                    }); 
                    window.location.reload();
                    setTimeout(function() {
                        jQuery.unblockUI();
                    }, 50);
                } else {
                    toastr.error('Silahkan periksa username dan password anda!', 'LOGIN GAGAL');
                    $("#username").val('');
                    $("#password").val('');
                    $("#username").focus();
                }
            }
        });
    }else if(uname == ''){
        $("#username").focus();
    }else{
        $("#password").focus();
    }
}
